#!/usr/bin/env python

import sys
sys.path.append("/home/des/nooshiri/phd_work/projects/relocation")

import glob
import os.path as op
from nlloc_core import NonLinLoc
from pyrocko.parimap import parimap


nll_locdir = sys.argv[1]
output_filename = sys.argv[2]

hypfiles = glob.glob(op.join(nll_locdir, '*[0-9]*.loc.hyp'))
equakes = parimap(lambda hf: NonLinLoc.read_nll_hyp(hf), sorted(hypfiles))


def magnitude_info(mag=None, mag_type=None, agency=None):
    if not mag:
        mag = " " * 4
    if not mag_type:
        mag_type = " "
    if not agency:
        agency = " " * 3

    return str("%s%s%s" % (mag, mag_type, agency))


with open(output_filename, 'w') as f:
    for eq in equakes:
        # Nordic format; type 1 line
        ot = eq.origin_time
        Year = ot.year
        Month = ot.month
        Day = ot.day
        Hour = ot.hour
        Minutes = ot.minute
        Seconds = ot.second + (ot.microsecond * 1.0e-6)
        DistInd = 'D'
        EventID = ' '
        Latitude = eq.latitude
        Longitude = eq.longitude
        Depth = eq.depth
        DepthInd = ' '
        LocInd = ' '
        Agency = 'NLL'
        NoStaUsed = eq.defining_phases
        RMS = eq.residual_rms
        Mag1 = magnitude_info()
        Mag2 = magnitude_info()
        Mag3 = magnitude_info()
        Ltype = "1"

        fmt = " %4d %2d%2d %2d%2d %4.1f %s%s%7.3f%8.3f%5.1f%s%s%s%3d%4.1f%s%s%s%s"
        f.write(str(fmt % (Year, Month, Day, Hour, Minutes, Seconds, DistInd,
                           EventID, Latitude, Longitude, Depth, DepthInd,
                           LocInd, Agency, NoStaUsed, RMS, Mag1, Mag2, Mag3,
                           Ltype)) + '\n\n')
