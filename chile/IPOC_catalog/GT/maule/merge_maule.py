import subprocess
import sys


maule = sys.argv[1]
single = sys.argv[2]
ssst = sys.argv[3]


# Merge Maule aftershocks catalog with our single event locations
with open('catmerge0.gmt', 'w') as outfile:
    subprocess.call(['perl', 'merge_seisan.pl', '-T=12', '-i', '-d=gmtd',
                     maule, single], stdout=outfile)

with open('catmerge0.xyz', 'w') as outfile:
    subprocess.call(['perl', 'merge_seisan.pl', '-T=12', '-i', '-d=xyz',
                     maule, single], stdout=outfile)


# Merge Maule aftershocks catalog with our global SSST locations
with open('catmerge1.gmt', 'w') as outfile:
    subprocess.call(['perl', 'merge_seisan.pl', '-T=5', '-D=50h', '-d=gmtd',
                     '-i', maule, ssst], stdout=outfile)

with open('catmerge1.xyz', 'w') as outfile:
    subprocess.call(['perl', 'merge_seisan.pl', '-T=5', '-D=50h', '-d=xyz',
                     '-i', maule, ssst], stdout=outfile)


# There must be more merged events in the two first files (Maule-Single)
# Find the common events!

cm0 = open('catmerge0.gmt', 'r').read().splitlines()
cm0_xyz = open('catmerge0.xyz', 'r').read().splitlines()
cm1 = open('catmerge1.gmt', 'r').read().splitlines()

idx1 = [3*i+1 for i in range(len(cm1)/3)]
cm0_new = []
cm0_xyz_new = []

for i in idx1:
    if cm1[i] in cm0:
        common_line = cm1[i]
        idx0 = cm0.index(common_line)
        cm0_new.append('>')
        cm0_new.append(cm0[idx0])
        cm0_new.append(cm0[idx0+1])
        cm0_xyz_new.append(cm0_xyz[idx0/3])

with open('catmerge0.gmt', 'w') as f:
    for line in cm0_new:
        f.write(line + '\n')

with open('catmerge0.xyz', 'w') as f:
    for line in cm0_xyz_new:
        f.write(line + '\n')
