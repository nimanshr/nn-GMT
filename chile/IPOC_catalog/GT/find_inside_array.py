#!/usr/bin/env python

import sys

import numpy as np

import pyproj
from shapely.geometry import Point, Polygon


# input files
ipoc_ctlg = sys.argv[1]
boundary_filename = sys.argv[2]
output_filename = sys.argv[3]
Mc = float(sys.argv[4])


# WGS84 datum
wgs84 = pyproj.Proj(init='epsg:4326')

# projection
npll = pyproj.Proj("+proj=latlon +lat_0=-28 +lon_0=-76 +x_0=0 +y_0=0 \
                    +ellps=WGS84 +datum=WGS84 +units=m +no_defs")

# define polygon
poly_lons, poly_lats = np.loadtxt(boundary_filename, usecols=(2, 1),
                                  unpack=True)

# transform polygon coordinates to lat/lon projection
poly_x, poly_y = pyproj.transform(wgs84, npll, poly_lons, poly_lats)
polygon = Polygon(zip(poly_x, poly_y))


# find the the epicenters located inside the polygon
ipoc_ctlg_select = open(output_filename, 'w')
with open(ipoc_ctlg, 'r') as f:
    lines = f.xreadlines()
    for line in sorted(lines):
        if line.endswith(" 1\n"):
            elat, elon, edep, emag = (float(line[23:30]), float(line[30:38]),
                                      float(line[38:42]), float(line[55:59]))
            ex, ey, ez = pyproj.transform(wgs84, npll, elon, elat, edep)
            p = Point(ex, ey, ez)
            if emag >= Mc and p.within(polygon):
                ipoc_ctlg_select.write(line + '\n')
        else:
            continue

ipoc_ctlg_select.close()
