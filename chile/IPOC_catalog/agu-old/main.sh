#!/bin/bash

gmtset FONT_ANNOT_PRIMARY 14P,31
gmtset FONT_LABEL 16p,31

# ############################################################################
# --- Input catalogs (Nordic format) ---
MINE=$1
IPOC=$2

PS=MERGE_CATALOG_SINGLE.ps
LEGEND="Single event"

# ############################################################################
# --- Boundary region ---
BOUND=./boundary.latlon

# --- Stations file ---
SFILE=./cx_stations.latlon

# --- Config ---
R1=-71/-68/-24/-17.5
J1=M2.6i
DX1=1.5i
DY1=5i

R2=-71/-68/0/200
J2=X2.6i/-1i
DY2=-1.2i

R3=0/200/-24/-17.5
J3=X1i/6i
DX3=2.8i
DY3=1.2i

# --- Symbols size and color ---
Sm=c0.15
Wm=thin,0

Si=a0.2
Gi=BLUE
Wi=thinnest,0

Ss=t0.5
Gs=RED

Wl=0.5,BLACK

# --- Wet and Dry regions color ---
WET=LIGHTBLUE
DRY=DARKGRAY

# ############################################################################
# --- Merge Catalogs ---
perl merge_seisan.pl -T=5 -d=gmtd $IPOC $MINE > catmerge.gmt
perl merge_seisan.pl -T=5 -d=xyz $IPOC $MINE > catmerge.xyz


# ############################################################################
# (1)--- Dray Lat-Lon map ---
pscoast -R$R1 -J$J1 -S$WET -G$DRY -Di -Ba1f0.5 -BeWNs -W -X$DX1 -Y$DY1 -P -K > $PS

grdimage ./topo/etopo1_chile.grd -I./topo/etopo1_chile.norm -R -J -C./topo/chile.cpt -K -O >> $PS
psbasemap -R -J -L-69.5/-23.65/-21.5/25 -O -K >> $PS

# Array boundary
awk '{print $3, $2}' $BOUND | psxy -R -J -Wthick -A -O -K >> $PS

# Add stations
awk '{print $3, $2}' $SFILE | psxy -R -J -S$Ss -G$Gs -Wthick -O -K >> $PS
awk '{print $3-0.06, $2+0.12, $1}' $SFILE | pstext -R -J -F+f8p,1+jTR -O -K >> $PS

# OFFSET lines
awk '{print $1, $2}' catmerge.gmt | psxy -R -J -W$Wl -A -O -K >> $PS

# IPOC hypocenters
awk '2 ==  NR % 3 {print $1, $2}' catmerge.gmt | psxy -R -J -S$Si -G$Gi -W$Wi -O -K >> $PS

# MINE hypocenters
awk '2 ==  (NR+2) % 3 {print $1, $2}' catmerge.gmt | psxy -R -J -S$Sm -W$Wm -O -K >> $PS

# LEGEND
cat > legend.txt << EOF
N 1
S 0.1i a 0.15i $Gi   0.25p 0.25i IPOC catalog
S 0.1i c 0.10i WHITE 0.45p 0.25i ${LEGEND} locations
EOF

gmt pslegend -R -J -Dx0.05c/14.15c/4.3c/BL -C0.1c/0.1c -L1.2 -F+p+gwhite legend.txt \
    -O -K --FONT_ANNOT_PRIMARY=10p,29 >> $PS


# ############################################################################
# (2)--- Draw Lon-Depth map
psbasemap -R$R2 -J$J2 -Bxa1f0.5+l"Longitude [deg]" -Bya50f25+l"Depth [km]" \
    -BWeSn -Y$DY2 -O -K >> $PS

# OFFSET lines
awk '{print $1, $3}' catmerge.gmt | psxy -R -J -W$Wl -A -O -K >> $PS

# IPOC hypocenters
awk '2 ==  NR % 3 {print $1, $3}' catmerge.gmt | psxy -R -J -S$Si -G$Gi -W$Wi -O -K >> $PS

# MINE hypocenters
awk '2 ==  (NR+2) % 3 {print $1, $3}' catmerge.gmt | psxy -R -J -S$Sm -W$Wm -O -K >> $PS


# ############################################################################
# (3)--- Draw Depth-Lat map
psbasemap -R$R3 -J$J3 -Bxa50f25+l"Depth [km]" -Bya1f0.5+l"Latitude [deg]" -BwEnS\
    -X$DX3 -Y$DY3 -O -K --MAP_ANNOT_ORTHO=wesn >> $PS

# OFFSET lines ---
awk '{print $3, $2, $1}' catmerge.gmt | awk '{print $1, $2'} | psxy -R -J \
    -W$Wl -A -O -K >> $PS

# IPOC hypocenters
awk '2 ==  NR % 3 {print $3, $2}' catmerge.gmt | psxy -R -J -S$Si -G$Gi -W$Wi -O -K >> $PS

# MINE hypocenters
awk '2 ==  (NR+2) % 3 {print $3, $2}' catmerge.gmt | psxy -R -J -S$Sm -W$Wm -O -K >> $PS


# ############################################################################
# Plot HISTOGRAM of offsets
Rh=-50/50/0/30
Jh=X1.4i/1i
DX=-2.8i
DY=-3.0i

N=`awk 'END {print NR}' catmerge.xyz`
FONT=+f14p,29,BLUE4+jTL

awk '{print $1}' catmerge.xyz | pshistogram -R$Rh -J$Jh -W5 -Z1 -Lthin,0 -GDARKGRAY \
    -Bpxa25f5+l"@%12%D@%%X [km]" -Bpya10f5+l"Percent [%]" -BWSne -X$DX -Y$DY -O -K \
    --MAP_ANNOT_ORTHO=wesn >> $PS

awk '{print $2}' catmerge.xyz | pshistogram -R$Rh -J$Jh -W5 -Z1 -Lthin,0 -GDARKGRAY \
    -Bpxa25f5+l"@%12%D@%%Y [km]" -Bpya10f5 -BewnS -X1.6i -O -K --MAP_ANNOT_ORTHO=wesn >> $PS

awk '{print $3}' catmerge.xyz | pshistogram -R$Rh -J$Jh -W5 -Z1 -Lthin,0 -GDARKGRAY \
    -Bpxa25f5+l"@%12%D@%%Z [km]" -Bpya10f5 -BewnS -X1.6i -O -K --MAP_ANNOT_ORTHO=wesn >> $PS

# --- Finalise ---
psxy -R -J -T -O >> $PS

# Delete redundant files
rm catmerge.*
rm legend.txt
rm gmt.conf
rm gmt.history

# Conver ps to png
ps2raster $PS -TG -P -A0.1

