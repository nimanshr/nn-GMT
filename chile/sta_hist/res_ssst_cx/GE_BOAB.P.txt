# Nsamples: 82  MAD:  1.109  mean: -0.517  variance:  3.472
gfz2010lepw   3.22490
gfz2010lluj   1.26610
gfz2010mncc   0.08750
gfz2010moyw  -1.30030
gfz2010nmpc  -0.98020
gfz2010nqwf  -0.87190
gfz2010nqwo  -1.19430
gfz2010nwef  -1.38990
gfz2010onma  -0.87990
gfz2010pqck  -2.06150
gfz2010pxgh  -1.84670
gfz2010rkfp   2.60270
gfz2010rqwj  -1.39410
gfz2010sfee   0.45850
gfz2010srdb   2.95060
gfz2010tcrl   1.67130
gfz2010upfb  -1.07710
gfz2010utcg  -1.10970
gfz2010xxzn  -4.22790
gfz2010yjev   0.26010
gfz2011adjs  -1.93010
gfz2011alip   0.60390
gfz2011aqxf  -0.26770
gfz2011ctbn   6.02000
gfz2011cykd  -2.02730
gfz2011cyre  -0.06040
gfz2011cyuk   1.65900
gfz2011dbwt  -1.62530
gfz2011dcqb  -0.93750
gfz2011dpqx  -1.86360
gfz2011ecaj  -1.07680
gfz2011enva   0.53160
gfz2011gbmx  -1.38940
gfz2011gkzt  -0.46610
gfz2011hfkj   0.11160
gfz2011hlqw  -1.30030
gfz2011hlsa  -1.16990
gfz2011hpgx  -2.51830
gfz2011hvbu   2.71490
gfz2011jplh  -0.55550
gfz2011jtlg   4.03230
gfz2011kxzp  -1.18810
gfz2011lzwj  -0.52210
gfz2011mdie  -1.76010
gfz2011ozsl  -0.49970
gfz2011rdby  -5.49280
gfz2011rewa  -1.72310
gfz2011rgwp  -0.70060
gfz2011taps   2.25670
gfz2011tbas  -0.69920
gfz2011tkfp  -1.93220
gfz2011udsq   2.29220
gfz2011uphh  -0.33370
gfz2011uzqv  -1.93240
gfz2012jlox  -0.48890
gfz2012lczo  -0.41940
gfz2012ldxg  -0.33050
gfz2012minr   3.75710
gfz2012oedb  -2.40750
gfz2012okhx  -0.56860
gfz2012peex  -0.87080
gfz2012ttoa   0.03240
gfz2012uafg  -2.11350
gfz2012wklq  -0.10580
gfz2012wvfl  -3.03160
gfz2012wypz  -0.25030
gfz2014lyhk   2.25520
gfz2014nnih  -3.63560
gfz2014nqfm  -0.15630
gfz2014nyko  -0.40200
gfz2014qlxd  -0.06820
gfz2014qngj  -1.65860
gfz2014qqhm  -1.00680
gfz2014raau  -3.80400
gfz2014sptq  -0.43930
gfz2014ssvz  -0.81580
gfz2015fkgt  -1.38400
gfz2015fsjj  -0.22120
gfz2015gckd  -0.50510
gfz2015gzja  -3.88910
gfz2015lhjy   0.11060
gfz2015lytv  -0.43110
