# Nsamples: 54  MAD:  0.525  mean: -0.246  variance:  0.432
gfz2010bxxs  -0.32200
gfz2010cqwl   0.06460
gfz2010grjd   0.74480
gfz2010nmpc  -0.70080
gfz2010usho   0.24850
gfz2011aatr  -0.31700
gfz2011enva  -0.69610
gfz2011hlqw  -0.01960
gfz2011hlsa  -0.23630
gfz2011lcxq  -0.04900
gfz2011lzwj  -0.67160
gfz2011rewa  -0.17550
gfz2011toub  -1.04430
gfz2011wxfs  -0.47820
gfz2012enno  -0.03540
gfz2012jlox  -0.17510
gfz2012kkup  -0.37160
gfz2012kudo  -0.34550
gfz2012ldxg   0.28470
gfz2012ttoa   0.16180
gfz2012wypz  -0.37090
gfz2013drxz  -0.07470
gfz2013jkut   0.19380
gfz2013kdeo   0.76990
gfz2013nwdi  -1.51040
gfz2013pfbx  -0.28400
gfz2013svci  -0.06450
gfz2013vjjc   1.24890
gfz2014ande  -2.05710
gfz2014bwue  -0.83400
gfz2014fgvi  -1.14780
gfz2014fhla   1.56220
gfz2014frdz   0.01820
gfz2014ftka   0.78370
gfz2014gkgf  -1.12190
gfz2014gkqf   0.22150
gfz2014gmge   0.88270
gfz2014gmhr  -1.73010
gfz2014gmnb  -0.43760
gfz2014gmnw  -0.77960
gfz2014goba  -0.49260
gfz2014gull   0.16520
gfz2014hasj  -0.09530
gfz2014jpep  -0.66660
gfz2014lgqn  -0.21610
gfz2014lxnc  -0.71940
gfz2014nqfm  -0.57300
gfz2014qqhm   0.01550
gfz2014ssvz   0.09900
gfz2015fsjj  -0.58790
gfz2015lhjy  -0.74470
gfz2015mpte  -0.25020
gfz2015svwm  -0.17230
gfz2015taml  -0.20320
