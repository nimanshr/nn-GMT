# Nsamples: 59  MAD:  0.627  mean: -0.262  variance:  0.399
gfz2012enno  -0.06330
gfz2012fzfa  -0.64350
gfz2012hnul  -1.06790
gfz2012jlox   0.04960
gfz2012kkup  -0.67830
gfz2012klhx  -0.18650
gfz2012kuct  -0.29090
gfz2012kudo  -0.09200
gfz2012ldxg   0.09390
gfz2012ldzj  -0.58620
gfz2012minr   0.23170
gfz2012teic  -0.24050
gfz2012ttoa  -0.03390
gfz2012uaqj  -0.09740
gfz2012vrgb   0.39460
gfz2012wypz   0.21490
gfz2013ccnr  -0.75470
gfz2013drxz  -0.57940
gfz2013jkut   0.26360
gfz2013kdeo  -0.05000
gfz2013ldqb  -0.63770
gfz2013mwbh  -0.56590
gfz2013nkgj  -2.37090
gfz2013nknw  -0.50050
gfz2013nwdi  -0.53770
gfz2013svci   0.37350
gfz2013uauo  -1.31490
gfz2013vjjc   1.03200
gfz2013xsdt  -0.38280
gfz2014bwue   0.13770
gfz2014fgvi  -0.74340
gfz2014fhla   0.98610
gfz2014fhrq  -0.73410
gfz2014ftka   0.38070
gfz2014gmge   1.06330
gfz2014gmhr   0.84700
gfz2014kitm  -0.03010
gfz2014lgqn  -0.53260
gfz2014nqfm  -0.72000
gfz2014oinz  -0.83950
gfz2014owxc  -0.76670
gfz2014qlxd  -0.21630
gfz2014qqhm   0.57120
gfz2014ssvz   0.46050
gfz2014vjoz  -0.44500
gfz2015berb  -0.24630
gfz2015chhn  -0.21130
gfz2015cyjd  -0.21480
gfz2015fsjj  -0.13770
gfz2015gckd  -0.10530
gfz2015ggjw  -0.05890
gfz2015lhjy   0.00250
gfz2015sfdd  -0.69620
gfz2015sfdy  -1.31930
gfz2015sfno  -1.11480
gfz2015sjbx  -0.93300
gfz2015snwh  -1.38010
gfz2015soxc  -0.11420
gfz2015taml   0.66100
