#!/bin/bash

ifile=$1

fname=$(basename "$ifile")
sta="${fname%.*.*}"

ffile=./final_res/$fname
cxfile=./cx_res/$fname

ps=${sta}.ps

Col=DARKGRAY

R=-10/10/0/35
J=X3.6i/1.8i

txt=+f14,4,MEDIUMBLUE+jTL


# ----------------------------------------------------------------------------
# histogram of NLLoc residuals
awk 'NR>1{print $2}' $ifile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,0 -G$Col -Bpya10f5+l"Frequency"+u"%" -Bpxa2f1 --FONT_LABEL=18p -BWS -P -X5.5 -Y20 -K > $ps

# add text
echo "-8 35 NLLoc Locations" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 35 N=`awk 'NR==1 {print $3}' $ifile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 30 MAD=`awk 'NR==1 {print $5}' $ifile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 25 @%12%m@%%=`awk 'NR==1 {print $7}' $ifile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 20 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ifile`" | pstext -R -J -F$txt -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of SSST residuals
awk 'NR>1{print $2}' $ffile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,0 -G$Col -Bpya10f5+l"Frequency"+u"%" -Bpxa2f1 --FONT_LABEL=18p -BWS -Y-2.6i -K -O >> $ps

# add text
echo "-8 35 SSST Locations" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 35 N=`awk 'NR==1 {print $3}' $ffile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 30 MAD=`awk 'NR==1 {print $5}' $ffile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 25 @%12%m@%%=`awk 'NR==1 {print $7}' $ffile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 20 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ffile`" | pstext -R -J -F$txt -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of SSST+CX residuals
awk 'NR>1{print $2}' $cxfile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,0 -G$Col -Bpya10f5+l"Frequency"+u"%" -Bpxa2f1+l"Travel-time residual [s]" --FONT_LABEL=18p -BWS -Y-2.6i -K -O >> $ps

# add text
echo "-8 35 SSST+CX Locations" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 35 N=`awk 'NR==1 {print $3}' $cxfile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 30 MAD=`awk 'NR==1 {print $5}' $cxfile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 25 @%12%m@%%=`awk 'NR==1 {print $7}' $cxfile`" | pstext -R -J -F$txt -N -O -K >> $ps
echo "4 20 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $cxfile`" | pstext -R -J -F$txt -N -O >> $ps


mv $ps ./figures_cx

