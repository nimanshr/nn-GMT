# Nsamples: 26  MAD:  0.311  mean: -0.242  variance:  0.246
gfz2010iull  -0.61490
gfz2010nmpc   0.16950
gfz2011aatr  -0.22050
gfz2011enva   0.37280
gfz2011hlqw  -0.75080
gfz2011hlsa  -0.33500
gfz2011lcxq   0.15030
gfz2011lzwj  -0.60440
gfz2011rewa  -0.06230
gfz2012enno  -0.09930
gfz2012jlox  -1.08760
gfz2012kkup  -0.08800
gfz2012klhx  -0.36330
gfz2012kudo  -0.03900
gfz2012wypz  -0.23640
gfz2013drxz  -0.07130
gfz2013nwdi  -0.25610
gfz2013svci  -0.63580
gfz2014fgvi  -0.74120
gfz2014gkgf  -0.87220
gfz2014gmhr   1.51230
gfz2015fsjj  -0.37490
gfz2015sfdd  -0.11570
gfz2015sfdy   0.00100
gfz2015snwh  -0.77900
gfz2015svwm  -0.15250
