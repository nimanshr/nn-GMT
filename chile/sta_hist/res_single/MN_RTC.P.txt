# Nsamples: 36  MAD:  1.109  mean:  0.493  variance:  0.904
gfz2010bxxs   0.10080
gfz2010cqwl   0.06520
gfz2010eisx   1.38340
gfz2010ekyc   1.23640
gfz2010eltf  -0.78620
gfz2010elyc   2.79630
gfz2010excp  -0.46190
gfz2010fedq   1.70790
gfz2010ffhq   0.39600
gfz2010grjd   1.22710
gfz2011aatr  -0.28350
gfz2011cykd   2.19670
gfz2011enva   1.57760
gfz2011gkzt   0.62980
gfz2011lcxq   1.09860
gfz2011lzwj   0.85620
gfz2011rewa   0.18790
gfz2011toub   0.19590
gfz2011wxfs   1.12130
gfz2012enno  -0.05550
gfz2012kkup   0.30280
gfz2012kudo   0.24730
gfz2013ccnr  -0.63180
gfz2013drxz  -0.12040
gfz2013nwdi  -0.51900
gfz2013qmer   1.09630
gfz2013svci   1.15210
gfz2013vjjc   1.48720
gfz2014fgvi   1.14500
gfz2014fhla   0.40770
gfz2014ftka  -2.23240
gfz2014gkgf  -0.18820
gfz2014gmhr  -0.36700
gfz2015chhn  -0.19220
gfz2015cyjd  -0.15880
gfz2015fsjj   1.11330
