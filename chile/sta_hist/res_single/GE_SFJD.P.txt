# Nsamples: 60  MAD:  0.569  mean: -1.044  variance:  1.064
gfz2010bxxs  -0.71120
gfz2010eaoy  -1.33860
gfz2010ebhc  -0.73490
gfz2010eisx  -0.25810
gfz2010ekyc  -0.83300
gfz2010excp  -3.78080
gfz2010fync  -2.30920
gfz2010grjd   1.16010
gfz2010iull  -1.26360
gfz2010nmpc  -0.76110
gfz2011aatr  -0.74560
gfz2011ccpw  -3.55370
gfz2011enva  -1.24440
gfz2011gkzt  -1.59350
gfz2011hlsa  -0.50730
gfz2011lcxq  -1.89950
gfz2011rewa  -1.29870
gfz2011toub  -0.73960
gfz2011wxfs  -0.70210
gfz2012enno  -0.86890
gfz2012hnul   1.86010
gfz2012jlox  -1.46150
gfz2012kkup  -0.20340
gfz2012kuct  -0.95780
gfz2012kudo  -1.11090
gfz2012ldxg  -0.88220
gfz2012ljql  -0.42220
gfz2012wypz  -0.70810
gfz2013ccnr  -1.24020
gfz2013jkut  -1.13130
gfz2013nkgj  -1.06700
gfz2013nwdi  -1.80740
gfz2013svci  -1.86320
gfz2013vjjc   1.18040
gfz2014bwue  -1.35770
gfz2014ejmt  -1.32050
gfz2014fgvi  -0.77430
gfz2014fhla  -1.81120
gfz2014fsuh   1.07020
gfz2014ftka  -2.54920
gfz2014ghpe  -2.06030
gfz2014gkgf  -0.40210
gfz2014gmge   0.68560
gfz2014gmhr   0.84200
gfz2014gmnb  -2.00240
gfz2014gmnw  -0.73200
gfz2014gnre   0.61790
gfz2014goba  -0.78330
gfz2014ssvz  -1.46370
gfz2014tqdo  -1.17400
gfz2014ubwn  -0.97290
gfz2015cyjd  -1.80650
gfz2015fsjj  -1.29800
gfz2015gckd  -0.80580
gfz2015lhjy  -0.99400
gfz2015lytv  -1.87470
gfz2015mpte  -1.56500
gfz2015sfdd  -2.03510
gfz2015sfdy  -2.04950
gfz2015svwm  -2.22150
