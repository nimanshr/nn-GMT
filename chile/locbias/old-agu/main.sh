#!/bin/bash

gmtset PS_MEDIA 5.9cx8.4c
gmtset PS_PAGE_ORIENTATION landscape
gmtset MAP_ORIGIN_X 0.5i
gmtset MAP_ORIGIN_Y 0.5i
gmtset FONT_ANNOT_PRIMARY 12p,0
gmtset FONT_LABEL 12p,0

PS=EXAMPLE.ps

R=-69.5/-68/-21.5/-20
J=M3.1i

cat >> locations.dat << EOF
-68.558350 -20.965576 119.213 ALLDATA
-69.023438 -21.210938 165.862 REGIONAL
-68.463135 -20.841064 121.169 TELESEIS
EOF

psbasemap -R$R -J$J -Ba0.5f0.25 -BWenS -X3i -Y2.5i -K --FORMAT_GEO_MAP=D > $PS
grdimage ./topo/etopo1_chile.grd -I./topo/etopo1_chile.norm -R -J -C./topo/chile.cpt -K -O >> $PS
psbasemap -R -J -L-68.25/-21.3/-21/20 -O -K >> $PS


# Add epicenters (
awk 'NR==1 {print $1, $2}' locations.dat | psxy -R -J -Sc0.3 -W2,BLACK -O -K >> $PS
awk 'NR==2 {print $1, $2}' locations.dat | psxy -R -J -Sc0.3 -W2,RED2 -O -K >> $PS
awk 'NR==3 {print $1, $2}' locations.dat | psxy -R -J -Sc0.3 -W2,GREEN4 -O -K >> $PS

# Add reference map
pscoast -Rg -JA-69/-20/1i -Bg45 -Dc -A5000 -Gburlywood4 -Swhite -X0.35 -Y2.2i -P -K -O >> $PS

# ADD A RECTANGLE AROUND STUDIED AREA
psxy -R -J -W2,blue3 -GNAVY -O -K >> $PS << EOF
-71.5 -18
-66.0 -18
-66.0 -23.5
-71.5 -23.5
-71.5 -18
EOF


# #############################################################################
# Plot residual histograms
Rh=0/100/-8/8
Jh=X1i/1i
S=c0.13
W=thinnest,0
XTICK=a30f10
XLABEL="Distance [deg]"
YTICK=a4f1
YLABEL="Residual [s]"

# ALL stations
psbasemap -R$Rh -J$Jh -Bpx$XTICK -Bpy$YTICK -BEwns \
    -X3.15i -Y0.1i -O -K >> $PS

psxy -R -J -W1,0 -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-ALL.dat | psxy -R$Rh -J$Jh -S$S -GBLACK -Wthinnest,GRAY30 \
    -O -K >> $PS

# NEAR-REGIONAL stations
psbasemap -R$Rh -J$Jh -Bpx$XTICK -Bpy$YTICK+l"$YLABEL" -BEwns \
    -Y-1.17i -O -K >> $PS

psxy -R -J -W1,0 -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-NEAR.dat | psxy -R$Rh -J$Jh -S$S -GRED2 -W$W \
    -O -K >> $PS

# TELESEISMIC stations
psbasemap -R$Rh -J$Jh -Bpx$XTICK+l"$XLABEL" -Bpy$YTICK -BEwnS \
    -Y-1.17i -O -K >> $PS

psxy -R -J -W1,0 -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-TELE.dat | psxy -R$Rh -J$Jh -S$S -GGREEN4 -W$W \
    -O -K >> $PS


# --- Finalize ---
psxy -R -J -T -O >> $PS

# convert ps to png
ps2raster $PS -TG -P -A0.1

