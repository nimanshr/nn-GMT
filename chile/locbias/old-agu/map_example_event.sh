#!/bin/bash

ps=example_location.ps
sShape=c0.25

#sta_P=recording_stations_P.lonlat
#sta_pP=recording_stations_pP.lonlat



###############################################################################
### ORIGINAL MAP ###
pscoast -R-174/-16/-172/-14r -JM4i -Df -A1000 -Ggoldenrod -S114/159/207 -Wthinnest -Ba0.5 --FORMAT_GEO_MAP=D --FONT_ANNOT_PRIMARY=16p -X5.0 -Y2.0 -K > $ps

grdimage ../grd_files/etopo1_0-15.grd -I../grd_files/etopo1_0-15.norm -R -J -C../CPT_files/cpt_city/Test.cpt -K -O >> $ps

psbasemap -R -J -L-173/-15.8/-15/25+lkm+jr -O -K --FONT_ANNOT_PRIMARY=16p >> $ps


### HYPOCENTER LOCATION USING CLOSE REGIONAL STATIONS ###
echo "-172.833  -15.057" | psxy -R -J -S$sShape -Gblack -K -O >> $ps
echo "-172.833  -15.057  \312" | pstext -R -J -F+f12p,34,ORANGE1 -K -O >> $ps


### HYPOCENTER LOCATION USING TELESEISMIC STATIONS (delta >= 30 degrees) ###
echo "-173.559  -15.277" | psxy -R -J -S$sShape -Gwhite -K -O >> $ps
echo "-173.559  -15.277  \313" | pstext -R -J -F+f12p,34,RED -K -O >> $ps


### HYPOCENTER LOCATION USING ALL STATIONS ###
echo "-173.482  -15.327" | psxy -R -J -S$sShape -Gwhite -K -O >> $ps
echo "-173.482  -15.327  \314" | pstext -R -J -F+f12p,34 -K -O >> $ps


### REFERENCE MAP ###
pscoast -Rg -JA170/0/1.2i -Bg45 -Dc -A5000 -Gburlywood4 -Swhite -X0.4 -Y2.7i -P -K -O >> $ps

### ADD A RECTANGLE AROUND STUDIED AREA ###
psxy -R -J -W2,blue3 -Gblue3 -O >> $ps << EOF
-175.0  -17.0
-175.0  -13.0
-171.0  -13.0
-171.0  -17.0
-175.0  -17.0
EOF


ps2raster $ps -Tf -A0.2 -P
