#!/bin/bash

gmtset PS_MEDIA 7.2cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/1.5p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

PS=INTRODUCTION.ps


# #############################################################################
#          MAP VIEW                                                           #
# #############################################################################
R=-69.25/-68/-21.5/-20.25
J=M5.4c
X0=1.4c
Y0=6c

DRY=150 #GRAY70
WET=235

Sm=c0.3
Sr=s0.3
St=d0.3
F=+f8p,Helvetica-Bold,BLACK

cat >> locations.dat << EOF
-68.558350 -20.965576 119.213 Mixed       Z=119 km
-69.023438 -21.210938 165.862 Regional    Z=165 km
-68.463135 -20.841064 121.169 Teleseismic Z=121 km
EOF


grdimage -R$R -J$J ../topo/chile.grd -I../topo/chile.norm  \
	 -C../topo/GMT_gray.cpt -X$X0 -Y$Y0 -K > $PS
psbasemap -R -J -Ba0.5f0.1 -BeWNs -L-68.2/-21.32/-21/20 -O -K \
          --FORMAT_GEO_MAP=D --MAP_TICK_LENGTH_PRIMARY=5p/2.5p --MAP_ANNOT_ORTHO=nsns >> $PS


# Add epicenters and Labels
# Mixed
awk 'NR==1 {print $1, $2}' locations.dat | psxy -R -J -S$Sm -W1.5 -O -K >> $PS
awk 'NR==1 {print $1+0.05, $2-0.02, $4}' locations.dat | pstext -R -J -F$F+jTL -O -K >> $PS
awk 'NR==1 {print $1+0.05, $2-0.08, $5, $6}' locations.dat | pstext -R -J -F$F+jTL -O -K >> $PS


# Regional (R)
awk 'NR==2 {print $1, $2}' locations.dat | psxy -R -J -S$Sr -W1.5 -O -K >> $PS
awk 'NR==2 {print $1+0.05, $2-0.02, $4}' locations.dat | pstext -R -J -F$F+jTL -O -K >> $PS
awk 'NR==2 {print $1+0.05, $2-0.08, $5, $6}' locations.dat | pstext -R -J -F$F+jTL -O -K >> $PS

# Teleseismic (T)
awk 'NR==3 {print $1, $2}' locations.dat | psxy -R -J -S$St -W1.5 -O -K >> $PS
awk 'NR==3 {print $1+0.05, $2+0.08, $4}' locations.dat | pstext -R -J -F$F+jBL -O -K >> $PS
awk 'NR==3 {print $1+0.05, $2+0.02, $5, $6}' locations.dat | pstext -R -J -F$F+jBL -O -K >> $PS


# Add reference map
pscoast -Rg -JA-69/-20/0.7i -Bg45 -Dc -A5000 -G$DRY -Swhite -X0.2c -Y3.8c -P -K -O >> $PS

# ADD A RECTANGLE AROUND STUDIED AREA
psxy -R -J -W2,blue3 -GNAVY -O -K >> $PS << EOF
-71.5 -18
-66.0 -18
-66.0 -23.5
-71.5 -23.5
-71.5 -18
EOF


# #############################################################################
#          RESIDUALS                                                          #
# #############################################################################
RR=0/100/-8/8
JR=X1.7c/1.7c
Sm=c0.13
Sr=s0.13
St=d0.13
G=100
W=thinnest,BLACK
XTICK=a30f10
XLABEL="Distance [deg]"
YTICK=a4f2
YLABEL="Residual [s]"

# Mixed
psbasemap -R$RR -J$JR -Bpx$XTICK -Bpy$YTICK+l"$YLABEL" -BeWnS+t"Mixed" -X-0.4c -Y-7.1c -O -K >> $PS
psxy -R -J -Wthinnest -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-ALL.dat | psxy -R -J -S$Sm -G$G -W$W -O -K >> $PS


# Regional
psbasemap -R -J -Bpx$XTICK+l"$XLABEL" -Bpy$YTICK -BewnS+t"Regional" -X2.05c -O -K >> $PS
psxy -R -J -Wthinnest -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-NEAR.dat | psxy -R -J -S$Sr -G$G -W$W -O -K >> $PS


# Teleseismic
psbasemap -R -J -Bpx$XTICK -Bpy$YTICK -BewnS+t"Teleseismic" -X2.05c -O -K >> $PS
psxy -R -J -Wthinnest -O -K >> $PS << EOF
0 0
100 0
EOF

awk '{print $2, $1}' Res-Dis-TELE.dat | psxy -R -J -S$St -G$G -W$W -O -K >> $PS


# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/8.5/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-4.6c -Y2c -O -K >> $PS << END
-1 31.0 (a)
-1  1.5 (b)
END



# --- Finalize ---
psxy -R -J -T -O >> $PS

# convert ps to png
psconvert $PS -Tf -P -A0.1

rm locations.dat gmt.conf gmt.history

