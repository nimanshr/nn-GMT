#!/bin/bash

e=$1
eC=$2   #ORANGE #SEAGREEN #VIOLETRED1 #THISTLE4
ps=$3

eS=c0.07
eW=thinnest,GRAY20

sP=./metadata/Pstations_noCX.lonlat
spP=./metadata/pPstations_noCX.lonlat
sS=t0.3
sCP=BLACK
sCpP=BLUE

# lat-lon map configuration
R1=-90.5/-62/-45.5/-5
J1=M4i

# lon-depth map configuration
R2=-90.5/-62/0/650
J2=X4i/-1.5i

# depth-lat map configuration
R3=0/650/-45.5/-5
J3=X1.5i/6.5i


# ---------------------- make lat-lon map ------------------------------------

pscoast -R$R1 -J$J1 -Gdarkgray -Slightblue -Ba5 -BWesN -X1i -Y4i -P -K > $ps
awk '{print $4, $5}' $e | psxy -R -J -S$eS -G$eC -W$eW -O -K >> $ps
awk '{print $2, $3}' $sP | psxy -R -J -S$sS -G$sCP -Wthinnest -O -K >> $ps
awk '{print $2-0.3, $3+0.3, $1}' $sP | pstext -R -J -F+f5p,Helvetica-Bold+jBR -Gwhite -O -K >> $ps
awk '{print $2, $3}' $spP | psxy -R -J -S$sS -G$sCpP -Wthinnest -O -K >> $ps


# ---------------------- make lon-depth map ----------------------------------

psbasemap -R$R2 -J$J2 -Bxa5+l"Longitude [deg]" -Bya100f50+l"Depth [km]" -BWeSn -Y-1.7i -O -K >> $ps
awk '{print $4, $6}' $e | psxy -R -J -S$eS -G$eC -W$eW -O -K >> $ps


# ---------------------- make depth-lat map ----------------------------------

psbasemap -R$R3 -J$J3 -Bxa100f50+l"Depth [km]" -Bya5+l"Latitude [deg]" -BwENs -X4.25i -Y1.7i -O -K --MAP_ANNOT_ORTHO=wesn >> $ps
awk '{print $6, $5}' $e | psxy -R -J -S$eS -G$eC -W$eW -O >> $ps
