#!/bin/bash

gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31

INPUT=$1

FNAME=$(basename "$INPUT")
STA="${FNAME%.*.*}"

ps=${STA}_P.ps
SSSTCOR=./data/${STA}.P.lonlat

SFILE=./metadata/stations.nll
Ss=t0.6
Gs=GRAY15
Se=c0.15
CPT=polar
T=-3/3/0.005


# #############################################################################
#      Config                                                                 #
# #############################################################################
# lat-lon map configuration
R1=-85/-62/-45.5/-10
J1=M3.2i

# lon-depth map configuration
R2=-85/-62/0/350
J2=X3.2i/-1.2i

# depth-lat map configuration
R3=0/350/-45.5/-10
J3=X1.2i/5.75i

# #############################################################################
#      Make a color pallete                                                   #
# #############################################################################
makecpt -C./cpt/BlRe.cpt -T$T > t.cpt


# #############################################################################
#      Map view (Lat-Lon)                                                     #
# #############################################################################
pscoast -R$R1 -J$J1 -GGRAY60 -Ba5 -Bwesn -N1 -Wthinnest -X3.5i -Y2.2i -K > $ps

# hypocenters
awk '{print $1,$2,$5}' $SSSTCOR | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps

# station
cat $SFILE | grep $STA | awk '{print $5,$4}' | psxy -R -J -S$Ss -G$Gs \
    -W1.5 -O -K >> $ps

# add colorbar
psscale -D0.1i/4.85i/1.5i/0.5 -B1f0.5:SSST\ \[s\]: -Ct.cpt --FONT_LABEL=16p,29 \
    --FONT_ANNOT_PRIMARY=12p,29 -O -K >> $ps


# #############################################################################
#      Cross section Lon-Depth                                                #
# #############################################################################
psbasemap -R$R2 -J$J2 -Bxa5+l"Longitude [deg]" -Bya100f50+l"Depth [km]" \
    -BWeSn -Y-1.35i -O -K >> $ps

# hypocenters
awk '{print $1,$3,$5}' $SSSTCOR | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps


# #############################################################################
#      Cross section Depth-Lat                                                #
# #############################################################################
psbasemap -R$R3 -J$J3 -Bxa100f50+l"Depth [km]" -Bya5+l"Latitude [deg]" -BwEnS \
    -X3.4i -Y1.35i -O -K --MAP_ANNOT_ORTHO=wens >> $ps

# hypocenters
awk '{print $3,$2,$5}' $SSSTCOR | psxy -R -J -S$Se -Ct.cpt -O -K >> $ps


# #############################################################################
#      Histograms                                                             #
# #############################################################################
SINGLE=./res_single/${STA}.P.txt
SSST=./res_ssst/${STA}.P.txt

R=-8/8/0/20
J=X2i/1.5i

W=0.1
Z=1
G=DARKGRAY
L=thin,BLACK

FONT=+f14p,29,BLUE4+jTL
XT=-7.5
YT=19.5
DY=3

YTICK=a5
YLABEL="Frequency [%]"
XTICK=a2f1
XLABEL="Residual [s]"


# ----------------------------------------------------------------------------
# histogram of initial residuals
awk 'NR>1{print $2}' $SINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+l"$YLABEL" -Bpx$XTICK+l"$XLABEL" -BeWnS -X-5.7i -Y4.2i -O -K >> $ps

# add text
echo "$XT $YT Single event loc." | pstext -R -J -F$FONT -O -K >> $ps
echo "$XT `echo $YT-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $SINGLE`" | pstext -R -J -F$FONT -O -K >> $ps
echo "$XT `echo $YT-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-4*$DY | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of final residuals
awk 'NR>1{print $2}' $SSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G \
    -Bpy$YTICK+l"$YLABEL" -Bpx$XTICK+l"$XLABEL" -BeWnS -Y-2.5i -K -O >> $ps

# add text
echo "$XT $YT SSST loc." | pstext -R -J -F$FONT -O -K >> $ps
echo "$XT `echo $YT-1*$DY | bc -l`  N=`awk 'NR==1 {print $3}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2*$DY | bc -l`  MAD=`awk 'NR==1 {print $5}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3*$DY | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-4*$DY | bc -l`  @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $SSST`" | pstext -R -J -F$FONT -N -O >> $ps




# convert to PNG
ps2raster $ps -Tf -E600 -P -A0.2


#mv *.png ./FIGS4AGU/pngFiles
#mv $ps ./FIGS4AGU/psFiles

