# Nsamples: 30  MAD:  0.937  mean: -0.926  variance:  1.382
gfz2010iull  -0.80210
gfz2010nmpc  -0.79160
gfz2011aatr  -1.91990
gfz2011enva  -0.62180
gfz2011hlsa  -1.69860
gfz2011lcxq   0.21430
gfz2011lzwj  -0.50840
gfz2011rewa  -0.52360
gfz2011wxfs  -0.95880
gfz2012jlox  -1.44550
gfz2012klhx  -1.27950
gfz2012wypz  -0.71190
gfz2013drxz  -1.92380
gfz2013jkut  -0.28020
gfz2013nwdi  -3.53980
gfz2013pfbx  -1.61630
gfz2013svci  -0.92830
gfz2014fgvi  -0.05520
gfz2014fhla   3.10740
gfz2014gkgf   0.12220
gfz2014gmhr   0.57950
gfz2014goba  -3.27060
gfz2014hasj  -1.54460
gfz2015chhn  -1.19710
gfz2015fsjj  -0.49680
gfz2015kxxs  -0.13950
gfz2015lhjy  -0.86940
gfz2015sfdd  -1.89560
gfz2015snwh  -0.93130
gfz2015svwm  -1.85400
