# Nsamples: 28  MAD:  0.562  mean: -0.331  variance:  0.760
gfz2010grjd   0.21530
gfz2011aatr  -0.56380
gfz2011enva  -0.52950
gfz2011gkzt   0.11200
gfz2011lcxq  -0.70400
gfz2011lzwj   0.10550
gfz2011rewa  -0.69330
gfz2011wxfs  -0.40730
gfz2012enno   0.08580
gfz2012jlox  -0.28270
gfz2012kkup  -0.15920
gfz2012ldxg   0.21100
gfz2012vrgb   0.69430
gfz2012wypz  -0.35960
gfz2013drxz  -0.32370
gfz2013jkut   0.70460
gfz2013kdeo  -0.65650
gfz2013svci  -0.01040
gfz2014bwue  -0.92370
gfz2014fgvi   0.51640
gfz2014ftka  -1.17730
gfz2014gkgf   0.05480
gfz2014gkqf  -3.54970
gfz2014hasj  -1.60430
gfz2014nqfm  -0.34620
gfz2015cyjd   1.49930
gfz2015lhjy  -0.63550
gfz2015mpte  -0.53560
