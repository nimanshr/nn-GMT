#!/bin/bash

input=$1

fname=$(basename "$input")
sta="${fname%.*.*}"

ps=${sta}_P.ps
corr=./data/${sta}.P.lonlat

sfile=./metadata/stations.nll
ssize=t0.8
C=polar
esize=c0.15

R=-90.5/-62/-45.5/-5
J=M5i

pscoast -R$R -J$J -Gdarkgray -Slightblue -Ba5 -X1.5i -Y2i -P -K > $ps

# make a color pallete
makecpt -C$C -T-5/5/0.1 > t.cpt

# add epicenters
awk '{print $1,$2,$4}' $corr | psxy -R -J -S$esize -Ct.cpt -Wthinnest -O -K >> $ps

# add the station
cat $sfile | grep $sta | awk '{print $5,$4}' | psxy -R -J -S$ssize -Gblack -W1,gray -O -K >> $ps

# add colorbar
psscale -D2.5i/-0.5i/5i/0.5h -B1f0.5:SSST\ \[s\]: -Ct.cpt --FONT_LABEL=20p,4 --FONT_ANNOT_PRIMARY=18p,4 -O >> $ps

# convert to PNG
ps2raster $ps -TG -P -A0.2

mv *.png figures/png/
mv $ps ./figures/ps/

