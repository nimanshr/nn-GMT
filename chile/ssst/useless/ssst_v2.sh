#!/bin/bash

input=$1

fname=$(basename "$input")
sta="${fname%.*.*}"

ps=${sta}_P.ps
corr=./data/${sta}.P.lonlat

sfile=./metadata/stations.nll
ssize=t0.8
Cpt=polar
esize=c0.1

# ----------------------------------------------------------------------------
# lat-lon map configuration
R1=-90.5/-62/-45.5/-5
J1=M4i

# lon-depth map configuration
R2=-90.5/-62/0/650
J2=X4i/-1.5i

# depth-lat map configuration
R3=0/650/-45.5/-5
J3=X1.5i/6.5i

# ----------------------------------------------------------------------------
# make a color pallete
makecpt -C$Cpt -T-5/5/0.1 > t.cpt

# ----------------------------------------------------------------------------
# make lat-lon map
pscoast -R$R1 -J$J1 -Gdarkgray -Slightblue -Ba5 -BWesN -X1i -Y4i -P -K > $ps

# hypocenters
awk '{print $1,$2,$5}' $corr | psxy -R -J -S$esize -Ct.cpt -Wthinnest -O -K >> $ps

# station
cat $sfile | grep $sta | awk '{print $5,$4}' | psxy -R -J -S$ssize -Gblack -W1,gray -O -K >> $ps

# ----------------------------------------------------------------------------
# make lon-depth map
psbasemap -R$R2 -J$J2 -Bxa5+l"Longitude [deg]" -Bya100f50+l"Depth [km]" -BWeSn -Y-1.7i -O -K >> $ps

# hypocenters
awk '{print $1,$3,$5}' $corr | psxy -R -J -S$esize -Ct.cpt -Wthinnest -O -K >> $ps

# ----------------------------------------------------------------------------
# make depth-lat map
psbasemap -R$R3 -J$J3 -Bxa100f50+l"Depth [km]" -Bya5+l"Latitude [deg]" -BwENs -X4.25i -Y1.7i -O -K --MAP_ANNOT_ORTHO=wesn >> $ps

# hypocenters
awk '{print $3,$2,$5}' $corr | psxy -R -J -S$esize -Ct.cpt -Wthinnest -O -K >> $ps

# ----------------------------------------------------------------------------
# add colorbar
psscale -D-1i/-2.6i/5i/0.5h -B1f0.5:SSST\ \[s\]: -Ct.cpt --FONT_LABEL=20p,4 --FONT_ANNOT_PRIMARY=18p,4 -O >> $ps
#psscale -D0.5i/-1.25i/2i/0.5 -B1f0.5:SSST\ \[s\]: -Ct.cpt --FONT_LABEL=20p,4 --FONT_ANNOT_PRIMARY=18p,4 -O >> $ps

# convert to PNG
ps2raster $ps -TG -P -A0.2

mv *.png figures_v2/png/
mv $ps ./figures_v2/ps/

