# Nsamples: 77  MAD:  1.648  mean: -0.693  variance:  2.894
gfz2013nwdi  -1.47100
gfz2013svci  -0.71460
gfz2013tusd  -3.30450
gfz2013vfyw  -0.99110
gfz2013vfzp   3.18400
gfz2013vjjc  -0.55670
gfz2013wjmm  -1.55910
gfz2013xmul  -3.20920
gfz2013xsdt  -3.09680
gfz2013ylbx  -0.50290
gfz2014afmx  -2.95450
gfz2014agrl   1.07490
gfz2014arnc  -0.27410
gfz2014bldx   2.12480
gfz2014bwue  -1.09430
gfz2014cqyo   0.84610
gfz2014dgiy  -0.45190
gfz2014fqpz   2.84040
gfz2014frdz   2.55470
gfz2014ftka  -2.22510
gfz2014gbcz  -1.13240
gfz2014gkgf  -2.67910
gfz2014gkmw   0.56100
gfz2014glvn  -0.55960
gfz2014gmge  -2.81420
gfz2014gmhr  -3.24220
gfz2014gmnw  -2.04230
gfz2014goba  -1.97610
gfz2014gxag   2.33760
gfz2014hssr   0.36100
gfz2014hvdm  -3.37880
gfz2014inbj  -2.65420
gfz2014jcjt  -0.28710
gfz2014jjhc  -1.67010
gfz2014jwmh  -1.14300
gfz2014khlk   1.73370
gfz2014lgqn  -1.02010
gfz2014owxc  -1.03740
gfz2014szzf  -0.67060
gfz2014uohh  -2.24660
gfz2014uvji   2.73100
gfz2014vhld  -1.97240
gfz2014watn  -0.73820
gfz2015dvbn  -2.48000
gfz2015fkgt  -1.50160
gfz2015fsjj  -3.02510
gfz2015gzja  -1.52890
gfz2015hpmk  -2.60590
gfz2015lhjy  -0.25340
gfz2015lytv  -0.52370
gfz2015mpte   0.03830
gfz2015nwug  -0.37300
gfz2015ocdz   1.00580
gfz2015sfdd  -0.49240
gfz2015sfdy   0.47930
gfz2015sfhz  -2.03550
gfz2015sfiq   0.08100
gfz2015sfks   1.50340
gfz2015sfnb   2.91140
gfz2015sfno   0.57620
gfz2015sfnv  -1.40930
gfz2015sfoc   0.09150
gfz2015sfoq   2.55020
gfz2015sgdn   1.94950
gfz2015shsz   1.73780
gfz2015sjgk  -2.38480
gfz2015sjoj  -1.33230
gfz2015sjvu  -1.25540
gfz2015slun  -0.79090
gfz2015smym  -0.68520
gfz2015snwh  -2.07560
gfz2015snyd  -0.10850
gfz2015soat  -1.23250
gfz2015soxc  -1.76910
gfz2015stfy  -0.86440
gfz2015svwm  -1.14530
gfz2015taml  -3.06520
