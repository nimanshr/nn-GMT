# Nsamples: 55  MAD:  0.499  mean: -0.340  variance:  0.829
gfz2010bxxs  -0.26900
gfz2010ftlm  -0.52840
gfz2010iull  -0.95220
gfz2010lepw   0.82680
gfz2010nlkh  -0.39030
gfz2010nmpc  -1.15680
gfz2011crdy  -2.48870
gfz2011enva  -0.18740
gfz2011gkzt  -0.77490
gfz2011lcxq   0.47610
gfz2011lzwj  -0.43310
gfz2011toub   0.68910
gfz2011wxfs  -0.44200
gfz2012dyyh  -0.60030
gfz2012jlox  -0.33350
gfz2012ldxg  -0.43560
gfz2012nojx   1.34260
gfz2012nsbz  -0.92450
gfz2012ttoa   0.07300
gfz2013jkut  -0.64140
gfz2013kdeo  -0.24410
gfz2013ldqb   1.15350
gfz2013mqmv   0.35260
gfz2013nkgj   2.34150
gfz2013nwdi  -0.50140
gfz2013pfbx  -0.68560
gfz2013svci  -0.60070
gfz2014algk  -0.58390
gfz2014fgvi  -1.01920
gfz2014fhla   1.41350
gfz2014frdz   2.02360
gfz2014ftka  -0.96030
gfz2014ghpe  -0.14400
gfz2014gkgf  -2.36280
gfz2014gkqf  -1.39460
gfz2014gmhr  -0.10810
gfz2014gmnb  -0.29150
gfz2014gmnw  -0.97570
gfz2014goba  -0.84180
gfz2014gull  -0.44450
gfz2014hqyx  -1.87860
gfz2014kbuk  -0.52740
gfz2014lgqn  -1.33110
gfz2014macx  -0.11790
gfz2014nqfm  -0.62050
gfz2014nrie  -0.74540
gfz2014qlxd   0.15910
gfz2014qqhm  -0.37560
gfz2014rdsn  -0.56190
gfz2014ubwn   0.02610
gfz2014vhms  -1.23670
gfz2015cyjd   1.14740
gfz2015gckd  -0.33940
gfz2015lhjy  -0.45960
gfz2015mpte  -0.83970
