# Nsamples: 93  MAD:  0.570  mean:  0.007  variance:  0.649
gfz2010lluj   1.27860
gfz2010nmpc  -0.12700
gfz2010nwef  -0.69570
gfz2010onma   0.28840
gfz2010rqwj   1.38630
gfz2010sfee   0.66450
gfz2010skqo   0.92840
gfz2010usho   0.81310
gfz2011aatr  -0.21900
gfz2011adjs   0.88820
gfz2011alip   0.03440
gfz2011bgrp  -0.69540
gfz2011cfae   0.99610
gfz2011cykd   1.30450
gfz2011dpqx  -1.78640
gfz2011enva  -0.39120
gfz2011gkzt  -1.10400
gfz2011hlsa  -0.07620
gfz2011jplh   0.09240
gfz2011lcxq   0.07000
gfz2011lzwj   0.32370
gfz2011mdie  -0.14830
gfz2011nqpd  -0.40590
gfz2011toub   0.13230
gfz2011tpfr   4.10150
gfz2011wxfs  -0.17430
gfz2011xyxj  -0.68090
gfz2012enno  -0.45140
gfz2012jlox  -0.05770
gfz2012kkup  -0.60670
gfz2012klhx  -0.32180
gfz2012kuct   0.51580
gfz2012kudo   0.15610
gfz2012ldxg  -0.37950
gfz2012ldzj  -0.25720
gfz2012minr   1.21090
gfz2012oazn  -0.28540
gfz2012teic  -0.59550
gfz2012ttoa  -0.12840
gfz2012uaqj  -0.46100
gfz2012vrgb   0.25700
gfz2012wypz   0.18260
gfz2013ccnr  -0.11580
gfz2013drxz   0.04950
gfz2013vjjc   1.32390
gfz2014afmx  -0.88740
gfz2014ande  -0.15060
gfz2014bwue   0.06470
gfz2014bzxy  -0.69810
gfz2014fgvi   0.92410
gfz2014fhla  -1.62640
gfz2014ftka  -0.75030
gfz2014gkgf  -1.68570
gfz2014gkhp  -1.97800
gfz2014gkqf  -0.41820
gfz2014glcu   0.94430
gfz2014gltx   0.13460
gfz2014gmge  -0.18590
gfz2014gmhr  -0.77970
gfz2014gmnb   0.75870
gfz2014gmnw   0.31170
gfz2014goba   0.66070
gfz2014gwab   0.37670
gfz2014gxxh   0.82420
gfz2014hgqo  -0.29860
gfz2014hjgr   0.29550
gfz2014hqxs   0.86920
gfz2014jjhc   0.51160
gfz2014lgqn   0.22680
gfz2014lxnc   0.42880
gfz2014lyhk  -0.03550
gfz2014nqfm   0.59490
gfz2014nrie  -0.07190
gfz2014oinz  -0.16360
gfz2014pvch   0.54580
gfz2014qlxd  -0.11610
gfz2014qqhm  -0.22320
gfz2014ssvz  -0.53170
gfz2014ubwn  -1.10220
gfz2015berb   0.32660
gfz2015cyjd   0.02220
gfz2015fsjj   0.03880
gfz2015gckd   0.77800
gfz2015hgbk  -0.21900
gfz2015hpmk  -0.76850
gfz2015lhjy   0.04040
gfz2015sfdd  -0.41190
gfz2015sfdy  -0.49690
gfz2015sjgk  -0.13140
gfz2015snwh  -0.33690
gfz2015soxc  -1.11460
gfz2015svwm  -0.42640
gfz2015taml  -0.25110
