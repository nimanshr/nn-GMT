# Nsamples: 39  MAD:  0.659  mean: -0.915  variance:  0.989
gfz2010grjd  -2.53960
gfz2010iull  -0.78218
gfz2010lluj  -1.09880
gfz2010ndxl   1.62570
gfz2010nmpc  -1.74900
gfz2011aatr  -1.49660
gfz2011enva  -0.26899
gfz2011hlqw  -1.45340
gfz2011hlsa  -1.69420
gfz2011lcxq  -1.76700
gfz2011lzwj  -1.29310
gfz2011rewa  -1.48280
gfz2011toub  -1.47160
gfz2011wxfs  -1.31640
gfz2012jlox  -1.89590
gfz2012kkup  -1.54300
gfz2012klhx  -0.66926
gfz2012kuct  -0.77273
gfz2012kudo  -1.60470
gfz2012vrgb  -0.93417
gfz2012wypz  -0.20522
gfz2012xssa   0.80429
gfz2013drxz  -1.32060
gfz2013nwdi  -1.35700
gfz2013svci  -1.23720
gfz2014bwue  -2.76020
gfz2014fgvi  -0.97894
gfz2014fhla  -0.57257
gfz2014gkgf  -0.22766
gfz2014gmhr  -0.45531
gfz2014goba  -0.76122
gfz2014oinz  -1.17840
gfz2014ssvz   0.63566
gfz2014veit  -2.47680
gfz2014vnxu   1.34690
gfz2015cyjd  -0.20870
gfz2015lhjy  -0.77647
gfz2015sfdd   1.27450
gfz2015sfdy  -1.01660
