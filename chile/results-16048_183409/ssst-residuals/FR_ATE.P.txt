# Nsamples: 89  MAD:  0.402  mean:  0.144  variance:  0.473
gfz2010ahqi  -0.03771
gfz2010bjeb  -0.02980
gfz2010bowy   0.43811
gfz2010boxo   0.18941
gfz2010bxxs  -0.06891
gfz2010cqwl   0.05301
gfz2010eaoy   0.67695
gfz2010earu   1.64430
gfz2010eawu   2.38580
gfz2010excp   0.28681
gfz2010fync   2.16280
gfz2010hrkr   0.24108
gfz2010iull   0.01401
gfz2010nmpc  -0.10708
gfz2010usho   0.41333
gfz2011aatr   0.10042
gfz2011dpqx  -0.31945
gfz2011enva   0.08714
gfz2011gkzt   0.92487
gfz2011hlqw  -0.18464
gfz2011hlsa  -0.16468
gfz2011jplh  -0.35280
gfz2011lcxq   0.81285
gfz2011lzwj   0.45264
gfz2011mdie  -0.33716
gfz2011owvz   0.86315
gfz2011rewa  -0.18063
gfz2011toub  -0.00000
gfz2011wxfs   0.34209
gfz2012fzfa  -0.78182
gfz2012hnul  -1.22190
gfz2012jlox   0.26862
gfz2012kkup  -0.28128
gfz2012klhx  -0.43410
gfz2012kuct  -0.04412
gfz2012kudo   0.08684
gfz2012ldxg  -0.55355
gfz2012ldzj   0.48153
gfz2012ttoa  -0.79397
gfz2012vrgb   0.50705
gfz2012wypz  -0.01380
gfz2013aqaw   0.07758
gfz2013ccnr  -0.00994
gfz2013drxz  -0.05718
gfz2013jkut   0.32132
gfz2013nkgj   0.30701
gfz2013nwdi  -0.83092
gfz2013qmer  -1.17280
gfz2013svci   0.09621
gfz2013vjjc   2.03410
gfz2013xsdt  -0.50513
gfz2014ande  -0.44516
gfz2014fgvi   0.18677
gfz2014fhla   0.95608
gfz2014fkmr  -0.79105
gfz2014ftka   0.81300
gfz2014gkgf   0.78188
gfz2014gkqf  -0.02130
gfz2014gmge   0.56504
gfz2014gmhr   1.69710
gfz2014gmnb  -0.30072
gfz2014goba   0.00000
gfz2014gull  -0.16009
gfz2014hasj   1.10030
gfz2014hocg  -0.08285
gfz2014itkw   0.60005
gfz2014jggm  -0.14716
gfz2014jjhc   0.20632
gfz2014jpep  -1.93500
gfz2014kitm   0.35714
gfz2014lgqn  -1.29790
gfz2014lxnc   0.09623
gfz2014lyhk  -0.14175
gfz2014lyhp   0.04871
gfz2014qqhm   0.21638
gfz2014ssvz   0.18262
gfz2014ubwn  -0.25952
gfz2015chhn  -0.29166
gfz2015cyjd  -0.09025
gfz2015fjhk   0.21224
gfz2015fsjj   0.45185
gfz2015kquq   0.40219
gfz2015lhjy   0.13067
gfz2015lytv   1.27650
gfz2015mpte   0.33314
gfz2015smym   0.00857
gfz2015snwh   0.19966
gfz2015soxc   0.44282
gfz2015tiyc  -0.23314
