#!/bin/bash

ifile=$1
ffile=$2
ps=$3

J=X3i/2i

Z=1

Li=thick,115
Gi=DARKGRAY
Lf=thick,ROYALBLUE4

Ylabel="Frequency"


# ---------------------- LONGITUDE UNCERTAINTIES -----------------------------
Rll=0/0.5/0/50
Wll=0.025
Xtickll=a0.25f0.1

Xlabellon="Longitude Uncertainty [deg]"

awk 'NR>1{print $1}' $ifile | pshistogram -R$Rll -J$J -W$Wll -Z$Z -L$Li -G$Gi -Bpya10f5+l$Ylabel+u"%" -Bpx${Xtickll}+l"$Xlabellon" -BWS -X1i -Y5i -K --FONT_LABEL=14p > $ps
awk 'NR>1{print $1}' $ffile | pshistogram -R -J -W$Wll -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
0.3 45
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
0.3 35
EOF

pstext -R -J -F+f14,29+jML -K -O >> $ps << EOF
0.32 45 NLLoc
0.32 35 SSST
EOF


# ---------------------- LATITUDE UNCERTAINTIES ------------------------------
Xlabellat="Latitude Uncertainty [deg]"

awk 'NR>1{print $2}' $ifile | pshistogram -R$Rll -J$J -W$Wll -Z$Z -L$Li -G$Gi -Bpya10f5+u"%" -Bpx${Xtickll}+l"$Xlabellat" -BWS -X3.7i -O -K --FONT_LABEL=14p >> $ps
awk 'NR>1{print $2}' $ffile | pshistogram -R -J -W$Wll -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
0.3 45
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
0.3 35
EOF

pstext -R -J -F+f14,29+jML -K -O >> $ps << EOF
0.32 45 NLLoc
0.32 35 SSST
EOF


# ---------------------- DEPTH UNCERTAINTIES ---------------------------------
Rz=0/80/0/25
Wz=4
Xtickz=a20f4
Xlabelz="Depth Uncertainty [km]"

awk 'NR>1{print $3}' $ifile | pshistogram -R$Rz -J$J -W$Wz -Z$Z -L$Li -G$Gi -Bpya5+u"%" -Bpx${Xtickz}+l"$Xlabelz" -BWS -X3.7i -O -K --FONT_LABEL=14p >> $ps
awk 'NR>1{print $3}' $ffile | pshistogram -R -J -W$Wz -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
50 22
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
50 17
EOF

pstext -R -J -F+f14,29+jML -K -O >> $ps << EOF
54 22 NLLoc
54 17 SSST
EOF


# ---------------------- MAX HORIZONTAL UNCERTAINTIES ------------------------
Rh=0/100/0/25
Wh=5
Xtickh=a20f5
Xlabelh="Maximum Horizontal Uncertainty [km]"

awk 'NR>1{print $4}' $ifile | pshistogram -R$Rh -J$J -W$Wh -Z$Z -L$Li -G$Gi -Bpya5+u"%" -Bpx${Xtickh}+l"$Xlabelh" -BWS -X-5.5i -Y-3.5i -O -K --FONT_LABEL=14p >> $ps
awk 'NR>1{print $4}' $ffile | pshistogram -R -J -W$Wh -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
60 22
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
60 17
EOF

pstext -R -J -F+f14,29+jML -K -O >> $ps << EOF
64 22 NLLoc
64 17 SSST
EOF


# ---------------------- RESIDUAL RMS-----------------------------------------
Rr=0/3/0/25
Wr=0.15
Xtickr=a0.6f0.15
Xlabelr="Residual RMS [s]"

awk 'NR>1{print $5}' $ifile | pshistogram -R$Rr -J$J -W$Wr -Z$Z -L$Li -G$Gi -Bpya5+u"%" -Bpx${Xtickr}+l"$Xlabelr" -BWS -X4i -O -K --FONT_LABEL=14p >> $ps
awk 'NR>1{print $5}' $ffile | pshistogram -R -J -W$Wr -Z$Z -L$Lf -O -K >> $ps

# Add Legend
psxy -R -J -Ss0.5 -G$Gi -W$Li -K -O >> $ps << EOF
1.9 22
EOF

psxy -R -J -Ss0.5 -W$Lf -K -O >> $ps << EOF
1.9 17
EOF

pstext -R -J -F+f14,29+jML -O >> $ps << EOF
2.05 22 NLLoc
2.05 17 SSST
EOF


