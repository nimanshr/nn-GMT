from __future__ import division
import sys

import numpy as np


def binned_stat(x, values, nbins):

    Bins = np.linspace(x.min(), x.max(), nbins+1)

    N, _ = np.histogram(x, bins=nbins)
    S, _ = np.histogram(x, bins=nbins, weights=values)
    S2, _ = np.histogram(x, bins=nbins, weights=values*values)

    Mean = np.divide(S, N)
    Std = np.sqrt(S2/N - Mean*Mean)

    return np.array([Bins[:-1], Mean, Std])

