#!/bin/bash

gmtset FONT_TITLE 20p,31
gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31
gmtset MAP_TITLE_OFFSET 8p

# ############################################################################
# Input residual files (columns=residual[s], distance[deg], azimuth[deg]) 
SINGLE=$1
SSST=$2

ps=RESIDUALS_FINAL.ps

# ############################################################################
# --- HISTOGRAMS ---
Rh=-5/5/0/10
J=X3i/2.5i

Wh=0.1
Gh=DARKGOLDENROD3
Lh=thinnest,BLACK

YTICK=a2
YLABEL="Frequency"
XTICK=a2f1
XLABEL="Travel-time residual [s]"

FONT=+f16p,29,0+jTL
XT=-4.5
YT=9.5

# ----------------------------------------------------------------------------
# --- Single Event Locations ---
awk 'NR>1{print $1}' $SINGLE | pshistogram -R$Rh -J$J -W$Wh -Z1 -L$Lh -G$Gh \
    -Bpy$YTICK+l"$YLABEL"+u"%" -Bpx$XTICK -BeWnS -X2.5 -Y10 -K > $ps

# add text
echo "$XT $YT  N=`awk 'NR==1 {print $3}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-1.2 | bc -l`  MAD=`awk 'NR==1 {print $5}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2.4 | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3.6 | bc -l`  @%12%s@%%=`awk 'NR==1 {print $9}' $SINGLE`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "4.5 9.5 a" | pstext -R -J -F+f18p,31,RED+jTR -GWHITE -O -K >> $ps


# --- SSST locations ---
awk 'NR>1{print $1}' $SSST | pshistogram -R$Rh -J$J -W$Wh -Z1 -L$Lh -G$Gh \
    -Bpy$YTICK+l$YLABEL+u"%" -Bpx$XTICK+l"$XLABEL" -BeWnS -Y-2.9i -O -K >> $ps

# add text
echo "$XT $YT    N=`awk 'NR==1 {print $3}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-1.2 | bc -l`  MAD=`awk 'NR==1 {print $5}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-2.4 | bc -l`  @%12%m@%%=`awk 'NR==1 {print $7}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "$XT `echo $YT-3.6 | bc -l`  @%12%s@%%=`awk 'NR==1 {print $9}' $SSST`" | pstext -R -J -F$FONT -N -O -K >> $ps
echo "4.5 9.5 a@+'@+" | pstext -R -J -F+f18p,31,RED+jTR -GWHITE -O -K >> $ps


# ############################################################################
# --- Residual-Distance figures ---
Rd=0/100/-5/5

S=c0.005
G=GRAY15

YTICK=a2f0.2
YLABEL="TT Residual [s]"

psbasemap -R$Rd -J$J -Bpxa20f5 -Bpy$YTICK+l"$YLABEL" -BeWnS -X3.9i -Y2.9i -O -K >> $ps
awk 'NR>1 {print $2, $1}' $SINGLE | psxy -R -J -S$S -G$G -O -K >> $ps

awk 'NR>1 {print $1, $2}' dist_binstat_single.dat | psxy -R -J -W1,RED -O -K >> $ps
awk 'NR>1 {print $1, $3}' dist_binstat_single.dat | psxy -R -J -W1,BLUE -O -K >> $ps
awk 'NR>1 {print $1, -$3}' dist_binstat_single.dat | psxy -R -J -W1,BLUE -O -K >> $ps

echo "90 4.6 b" | pstext -R -J -F+f18p,31,RED+jTL -GWHITE -O -K >> $ps


psbasemap -R$Rd -J$J -Bpxa20f5+l"Epicentral distance [deg]" -Bpy$YTICK+l"$YLABEL" -BeWnS -Y-2.9i -O -K >> $ps
awk 'NR>1 {print $2, $1}' $SSST | psxy -R -J -S$S -G$G -O -K >> $ps

awk 'NR>1 {print $1, $2}' dist_binstat_ssst.dat | psxy -R -J -W1,RED -O -K >> $ps
awk 'NR>1 {print $1, $3}' dist_binstat_ssst.dat | psxy -R -J -W1,BLUE -O -K >> $ps
awk 'NR>1 {print $1, -$3}' dist_binstat_ssst.dat | psxy -R -J -W1,BLUE -O -K >> $ps

echo "90 4.6 b@+'@+" | pstext -R -J -F+f18p,31,RED+jTL -GWHITE -O -K >> $ps


# ############################################################################


# --- Finalize ---
psxy -R -J -T -O >> $ps


# --- Convert ps file ---
ps2raster $ps -Tg -P -E200 -A0.1

