#!/bin/bash

ps=CATALOG_RES_HIST.ps

ifile=./init_catalog_res.txt
ffile=./final_catalog_res.txt

Col=39/64/139 #46/139/87 #69/139/0

R=-5/5/0/20
J=X3.6i/1.8i

itxt=+f14,4,MEDIUMBLUE+jTL
ftxt=+f14,4,RED3+jTL


# ----------------------------------------------------------------------------
# histogram of initial residuals
awk 'NR>1{print $1}' $ifile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,GRAY20 -G$Col -Bpya5f5+l"Frequency"+u"%" -Bpxa2f1 --FONT_LABEL=18p -BWS -P -X5.5 -Y17 -K > $ps

# add text
echo "-4.5 20 NLLoc Locations" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "3 20 N=`awk 'NR==1 {print $3}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "3 17 MAD=`awk 'NR==1 {print $5}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "3 14 @%12%m@%%=`awk 'NR==1 {print $7}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps
echo "3 11 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ifile`" | pstext -R -J -F$itxt -N -O -K >> $ps

# ----------------------------------------------------------------------------
# histogram of final residuals
awk 'NR>1{print $1}' $ffile | pshistogram -R$R -J$J -W0.2 -Z1 -Lthin,GRAY20 -G$Col -Bpya5f5+l"Frequency"+u"%" -Bpxa2f1+l"Travel-time residual [s]" --FONT_LABEL=18p -BWS -Y-2.6i -K -O >> $ps

# add text
echo "-4.5 20 SSST Locations" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "3 20 N=`awk 'NR==1 {print $3}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "3 17 MAD=`awk 'NR==1 {print $5}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "3 14 @%12%m@%%=`awk 'NR==1 {print $7}' $ffile`" | pstext -R -J -F$ftxt -N -O -K >> $ps
echo "3 11 @%12%s@%%@+2@+=`awk 'NR==1 {print $9}' $ffile`" | pstext -R -J -F$ftxt -N -O >> $ps
