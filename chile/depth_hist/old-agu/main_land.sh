#!/bin/bash

gmtset FONT_TITLE 20p,31
gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31
gmtset MAP_TITLE_OFFSET 8p

ps=DEPTH_HIST_LAND.ps

LOCGFN=$1      # geofon
LOCSINGLE=$2   # nlloc
LOCSSST=$3     # ssst


R=0/300/0/30
J=X3i/2.5i

W=10
Z=1

G=85/26/139
L=thick,GRAY40

Ytick=a10f5
Ylabel="Frequency"
Xtick=a50f10
Xlabel="Depth [km]"


# ---------------------- histograms ----------------------
awk '{print $6}' $LOCGFN | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+l$Ylabel+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"GEOFON bulletin data" -X2.4 -Y5 -K > $ps

awk '{print $6}' $LOCSINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"Single event locations" -X3.7i -O -K >> $ps

awk '{print $6}' $LOCSSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"SSST locations" -X3.7i -O >> $ps

# --- convert ps file
ps2raster $ps -TG -E600 -P -A0.2

