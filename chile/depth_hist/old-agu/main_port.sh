#!/bin/bash


gmtset FONT_TITLE 18p,Helvetica-Bold
gmtset FONT_ANNOT_PRIMARY 16p
gmtset FONT_LABEL 18p,Helvetica

ps=DEPTH_HIST_PORT.ps

LOCGFN=$1      # geofon
LOCSINGLE=$2   # relocated
LOCSSST=$3     # ssst


R=0/300/0/30
J=X3i/2i

W=10
Z=1

G=85/26/139
L=thick,GRAY40

Ytick=a10f5
Ylabel="Frequency"
Xtick=a50f10
Xlabel="Depth [km]"


# ---------------------- histograms ----------------------
awk '{print $6}' $LOCGFN | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+l$Ylabel+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"GEOFON locations" -X3i -Y3i -P -K > $ps

awk '{print $6}' $LOCSINGLE | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"NLLoc locations" -Y3i -O -K >> $ps

awk '{print $6}' $LOCSSST | pshistogram -R$R -J$J -W$W -Z$Z -L$L -G$G -Bpy$Ytick+u"%" -Bpx$Xtick+l"$Xlabel" -BeWnS+t"SSST locations" -Y3i -O >> $ps

# --- convert ps file
ps2raster $ps -TG -P -A0.2

