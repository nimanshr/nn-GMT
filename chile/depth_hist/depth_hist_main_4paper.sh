#!/bin/bash

gmtset PS_MEDIA 15.6cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

LOCFILE0=../results-16048_183409/geofon-locations.lonlat
TITLE0="GEOFON Catalog"
LOCFILE1=../results-16048_183409/single-locations.lonlat
TITLE1="Single-Event Locations"
LOCFILE2=../results-16048_183409/ssst-locations.lonlat
TITLE2="Global SSST Locations"


ps=CHILE-DEPTH-HIST.ps


# #############################################################################
#          DEPTH HISTOGRAMS >>> CONFIG                                        #
# #############################################################################
R=0/350/0/35
J=X4.2c/4c

XTICK=a100f20
YTICK=a10f5

W=10
Z=1
G=150 #dodgerblue4
L=thin,25 #deepskyblue4

# #############################################################################
#          DEPTH HISTOGRAMS >>> PLOT                                          #
# #############################################################################

# --- Initialize ---
psxy -R$R -J$J -T -X0 -Y0 -K > $ps

XOFFSET=(1.2c 4.9c 4.9c)
YOFFSET=(5c 0 0)

for i in 0 1 2;do
	X=${XOFFSET[$i]}
	Y=${YOFFSET[$i]}
	LOCFILE=`eval "echo $"LOCFILE$i""`
	TITLE=`eval "echo $"TITLE$i""`

	if [ $i = 0 ];then
		Bpy=$YTICK+l"Frequency [%]"
		ANNOT=eWnS
	else
		By=$YTICK
		ANNOT=ewnS
	fi

	awk '{print $6}' $LOCFILE | pshistogram -R$R -J$J -W$W -Z$Z -G$G -L$L \
		-Bpx$XTICK+l"Depth [km]" -Bpy"$Bpy" -B$ANNOT+t"$TITLE" -X$X -Y$Y -O -K >> $ps
done



# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert PS to PDF ---
psconvert $ps -Tf -P -A0.2


rm gmt.conf gmt.history

