#!/bin/bash

ps=offset_StoCX_zoomX.ps
lonz=./data_zoomX/offset_StoCX_zoomX.lonz
latz=./data_zoomX/offset_StoCX_zoomX.latz

esize=c0.12
ecolor1=SEAGREEN
ecolor2=GOLD

Rlon=-75/-70/0/220
Rlat=-45/-14.5/0/220
J=X8i/-2.5i


psbasemap -R$Rlon -J$J -Bxa2.5+l"Longitude [deg]" -Bya20+l"Depth [km]" -BWeSn -X2i -Y5i -K > $ps
awk '{print}' $lonz | psxy -R -J -Wthinnest -N -O -K >> $ps

awk '1 == NR % 3' $lonz | psxy -R -J -S$esize -G$ecolor1 -Wthinnest -O -K >> $ps
awk '1 == (NR+2) % 3' $lonz | psxy -R -J -S$esize -G$ecolor2 -Wthinnest -O -K >> $ps



psbasemap -R$Rlat -J$J -Bxa2.5+l"Latitude [deg]" -Bya20+l"Depth [km]" -BWeSn -Y-4i -O -K >> $ps
awk '{print}' $latz | psxy -R -J -Wthinnest -N -O -K >> $ps

awk '1 == NR % 3' $latz | psxy -R -J -S$esize -G$ecolor1 -Wthinnest -O -K >> $ps
awk '1 == (NR+2) % 3' $latz | psxy -R -J -S$esize -G$ecolor2 -Wthinnest -O -K >> $ps
