#!/bin/bash

gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31

LOCFILE=$1

PS=CROSS_SECT_SSST.ps

# LatLon map config
RM=-80/-62/-40/-14
JM=M2.8i
DRY=DARKGRAY
WET=SKYBLUE

# Cross-section maps config
RC=-80/-62/-10/350
JC=X2.8i/-1.2i


Ge=GREEN4
Se=c0.09
We=thinnest,GRAY10


# #################### Draw the LatLon map #######################################
pscoast -R$RM -J$JM -G$DRY -S$WET -Ba5 -BEWNs -L-65/-38/-27/100 -Di -N1 -Wthinnest -X2.5i -Y6.7i -P -K > $PS

# Add earthquake epicenters
awk '{print $4,$5}' $LOCFILE | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS

# Draw cross-section rectangles
for i in `seq -18 -5 -33`;do
    psxy -R -J -Sj -Wthick,RED2 -N -O -K >> $PS <<EOF
    -71  $i  0  2.9i  1
EOF
done

# Add profile labels
N=1
for i in `seq -18 -5 -33`;do
    pstext -R -J -F+f16p,31,RED2+j -N -O -K >> $PS << EOF
    -81.7  $i  ML  $N
    -60.3  $i  MR  $N@+'@+
EOF
N=`expr $N + 1`
done


# #################### Draw the cross-section maps ###########################
N=1
for i in `seq -18 -5 -33`;do
awk '{print $4,$5,$6}' $LOCFILE | project -C-80/$i -E-62/$i -W-1/1 -Fxz > proj$N.dat
N=`expr $N + 1`
done

# Draw top profile map
psbasemap -R$RC -J$JC -Bxa5 -Bya100f50 -BeWns -Y-1.6i -P -O -K >> $PS
awk '{print}' proj1.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS
pstext -R -J -F+f16p,31,RED2+j -N -O -K >> $PS << EOF
-79.5  -20  BL  1
-62.5  -20  BR  1@+'@+
EOF

# Draw bottom profile maps
N=2
for i in `seq -23 -5 -33`;do
if [ $i = -33 ];then
    psbasemap -R$RC -J$JC -Bxa5+l"Longitude [deg]" -Bya100f50 -BeWnS -Y-1.45i -O -K >> $PS
else
    psbasemap -R$RC -J$JC -Bxa5 -Bya100f50 -BeWns -Y-1.45i -O -K >> $PS
fi
awk '{print}' proj$N.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS
pstext -R -J -F+f16,31,RED2+j -N -O -K >> $PS << EOF
-79.5  -20  BL  $N
-62.5  -20  BR  $N@+'@+
EOF
N=`expr $N + 1`
done



# --- Finalize ---
pstext -R -J -F+f20p,31+a90+j -N -O >> $PS <<EOF
-85  -700  MR  Depth [km]
EOF

# remove redundant files
rm proj[0-9].dat

# Convert PS file to PNG
psconvert $PS -TG -E600 -P -A0.2

