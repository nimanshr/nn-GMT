#!/bin/bash

eq=$1
ecol=$2
ps=$3


esize=c0.07

Rpro=-90.5/-62/0/650
Jpro=X4i/-1.25i


###################
for i in `seq -18 -5 -38`;do
awk '{print $4,$5,$6}' $eq | project -C-90.5/$i -E-62/$i -W-1/1 -Fxz > prof$i.dat
done

psbasemap -R$Rpro -J$Jpro -Bxa5 -Bya100f50+l"Depth [km]" -BWeSn -X2.5i -Y9.5i -P -K > $ps
awk '{print}' prof-18.dat | psxy -R -J -S$esize -G$ecol -Wthinnest -N -O -K >> $ps
pstext -R -J -F+f14,31,+j -N -O -K >> $ps << EOF
-90.5  -50  BL  1
-62.0  -50  BR  1@+'@+
EOF


j=2
for i in `seq -23 -5 -38`;do
if [ $i = -38 ];then
    psbasemap -R$Rpro -J$Jpro -Bxa5+l"Longitude [deg]" -Bya100f50+l"Depth [km]" -BWeSn -Y-2i -O -K >> $ps
else
    psbasemap -R$Rpro -J$Jpro -Bxa5 -Bya100f50+l"Depth [km]" -BWeSn -Y-2i -O -K >> $ps
fi
awk '{print}' prof$i.dat | psxy -R -J -S$esize -G$ecol -Wthinnest -N -O -K >> $ps
pstext -R -J -F+f14,31,+j -N -O -K >> $ps << EOF
-90.5  -50  BL  $j
-62.5  -50  BR  $j@+'@+
EOF
j=`expr $j + 1`
done

# Finalize
psxy -R -J -T -O >> $ps

# convert to PNG
ps2raster $ps -TG -P -A0.2

# move figures
if [ ! -d "psfigs" ];then
    mkdir psfigs
fi
mv *.ps psfigs/

if [ ! -d "pngfigs" ];then
    mkdir pngfigs
fi
mv *.png pngfigs

# remove redundant files
rm prof*.dat

