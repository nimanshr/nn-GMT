#!/bin/bash

gmtset FONT_ANNOT_PRIMARY 16p,31
gmtset FONT_LABEL 20p,31

LOCFILE1=$1
LOCFILE2=$2

PS=CROSS_SECT_2.ps

# LatLon map config
RM=-80/-62/-40/-14
JM=M2.8i
DRY=DARKGRAY
WET=LIGHTBLUE

# Cross-section maps config
RC=-80/-62/-10/350
JC=X2.8i/-1.1i


Ge=GREEN4
Se=c0.07
We=thinnest,GRAY10

DX1=0
DY1=5.5i
DX2=3.5i
DY2=3.65i

LATPROF=(-23.7 -28.2 -32)


# ############################################################################
# --- Initialize ---
psxy -R$RM -J$JM -T -P -K > $PS


# --- Main ---
for k in 1 2;do

X=`eval "echo $"DX$k""`
Y=`eval "echo $"DY$k""`
DATA=`eval "echo $"LOCFILE$k""`

if [ $k = 1 ];then
    Bm=eWNs
else
    Bm=EwNs
fi

# ########## Draw the LatLon map ##########
pscoast -R$RM -J$JM -G$DRY -S$WET -Ba5 -B$Bm -Di -N1 -Wthinnest -X$X -Y$Y -O -K >> $PS
grdimage ../topo/etopo1_chile.grd -I../topo/etopo1_chile.norm -R -J -C../topo/chile.cpt -K -O >> $PS
psbasemap -R -J -L-65/-38/-27/100 -O -K >> $PS

# Add earthquake epicenters
awk '{print $4,$5}' $DATA | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS

# Draw cross-section rectangles
for i in "${LATPROF[@]}";do
    psxy -R -J -Sj -Wthick,RED2 -N -O -K >> $PS <<EOF
    -71  $i  0  2.9i  1
EOF
done

# Add profile labels
N=1
for i in "${LATPROF[@]}";do
    pstext -R -J -F+f16p,31,RED2+j -N -O -K >> $PS << EOF
    -81.7  $i  ML  $N
    -60.3  $i  MR  $N@+'@+
EOF
N=`expr $N + 1`
done


# ########## Draw the cross-section maps ##########
N=1
for i in "${LATPROF[@]}";do
awk '{print $4,$5,$6}' $DATA | project -C-80/$i -E-62/$i -W-1/1 -Fxz > proj$N.dat
N=`expr $N + 1`
done

# Draw top profile map
psbasemap -R$RC -J$JC -Bxa5 -Bya100f50 -BeWns -Y-1.25i -P -O -K >> $PS
awk '{print}' proj1.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS
pstext -R -J -F+f16p,31,RED2+j -N -O -K >> $PS << EOF
-79.5  15  TL  1
-62.5  15  TR  1@+'@+
EOF

# Draw bottom profile maps
for i in 2 3;do
if [ $i = 3 ];then
    psbasemap -R$RC -J$JC -Bxa5+l"Longitude [deg]" -Bya100f50 -BeWnS -Y-1.2i -O -K >> $PS
else
    psbasemap -R$RC -J$JC -Bxa5 -Bya100f50 -BeWns -Y-1.2i -O -K >> $PS
fi
awk '{print}' proj$i.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $PS
pstext -R -J -F+f16,31,RED2+j -N -O -K >> $PS << EOF
-79.5  15  TL  $i
-62.5  15  TR  $i@+'@+
EOF
done



# --- Add Depth label ---
if [ $k = 1 ];then
    pstext -R -J -F+f20p,31+a90+j -N -O -K >> $PS <<EOF
    -85  -525  MR  Depth [km]
EOF
fi

done


# --- Finalize ---
psxy -R -J -T -O >> $PS

# remove redundant files
rm proj[0-9].dat

# Convert PS file to PNG
ps2raster $PS -TG -P -A0.1

