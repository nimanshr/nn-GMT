#!/bin/bash

e=$1
eC=$2
ps=$3

eS=c0.07
eW=thinnest,GRAY20

sP=./metadata/Pstations_noCX.lonlat
spP=./metadata/pPstations_noCX.lonlat
sS=t0.3
sCP=BLACK
sCpP=BLUE

R=-90.5/-62/-45.5/-5
J=M4i #C-76/-25/5i


pscoast -R$R -J$J -Gdarkgray -Slightblue -Ba5 -X2i -Y3i -P -K > $ps


awk '{print $4,$5}' $e | psxy -R -J -S$eS -G$eC -W$eW -O -K >> $ps

awk '{print $2, $3}' $sP | psxy -R -J -S$sS -G$sCP -Wthinnest -O -K >> $ps
awk '{print $2, $3}' $spP | psxy -R -J -S$sS -G$sCpP -Wthinnest -O -K >> $ps

# Draw cross-section lines
for i in `seq -18 -5 -38`;do
    psxy -R -J -Wthick -A -O -K >> $ps << EOF
    -90.5  $i
    -62.0  $i
EOF

    psxy -R -J -Sc0.001 -G0 -O -K -Ey/thick >> $ps <<EOF
    -76.25  $i  1
EOF
done

# Add line's labels
j=1
for i in `seq -18 -5 -38`;do
    l=`expr $i + 1`
    pstext -R -J -F+f12,31,+j -Gwhite -O -K >> $ps << EOF
    -90.0  $l  BL  $j
    -62.5  $l  BR  $j@+'@+
EOF
j=`expr $j + 1`
done

# Finalize
psxy -R -J -T -O >> $ps

# Convert to PNG
ps2raster $ps -TG -P -A0.2

# move figures
if [ ! -d "psfigs" ];then
    mkdir psfigs
fi
mv *.ps psfigs/

if [ ! -d "pngfigs" ];then
    mkdir pngfigs
fi
mv *.png pngfigs/

