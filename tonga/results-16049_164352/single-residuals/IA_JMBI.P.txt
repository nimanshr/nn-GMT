# Nsamples: 62  MAD:  0.858  mean:  1.723  std:  0.823
gfz2010aseg   2.58380
gfz2010bklj   1.58670
gfz2010btme   0.96382
gfz2010bytu   1.45050
gfz2010ccye   1.96590
gfz2010clbw   1.40240
gfz2010cllh   3.13910
gfz2010cpig   2.35320
gfz2010crgy   0.48884
gfz2010crir   0.34175
gfz2010dask   2.49880
gfz2010djib   0.53882
gfz2010fjme   3.21810
gfz2010habf   1.74890
gfz2010iaay   2.48730
gfz2010izag   2.88030
gfz2010jlvp   1.54120
gfz2010mdti   1.83870
gfz2010mqzz   3.15190
gfz2010ngge   2.34990
gfz2010okpc   0.76418
gfz2010qdqk   1.89890
gfz2010rljv   2.63840
gfz2010rpjd  -0.02124
gfz2010tmba   1.95730
gfz2010tzma   0.83891
gfz2010ugws   2.82530
gfz2010uhjy   2.16190
gfz2010uidg   0.45935
gfz2010vong   2.15620
gfz2010xpzq   2.21410
gfz2011ahvh   1.87120
gfz2011bpqm   1.19810
gfz2011bygs   2.12590
gfz2011cdge   4.27120
gfz2011dabj   1.05950
gfz2011dpyu   0.80858
gfz2011ndhh   1.45490
gfz2011nijp   0.93475
gfz2011nuqo   1.35960
gfz2011oslk   1.06770
gfz2011taqr   1.52080
gfz2011uqry   1.21620
gfz2011vtzv   1.67740
gfz2013byyy   1.21480
gfz2013dfvx   0.90066
gfz2013jobl   1.24220
gfz2013kats   1.11710
gfz2013kbbg   1.73720
gfz2013kbbn   2.48760
gfz2013kcdb   2.79940
gfz2013kjgh   1.18050
gfz2013lbvl   0.91305
gfz2013oizq   2.13520
gfz2013qigb   1.22250
gfz2013qoak   1.04100
gfz2013skvq   2.00740
gfz2013tdsx   2.19930
gfz2013utqa   1.13960
gfz2013uxzw   2.11800
gfz2013vojy   2.04660
gfz2013vvre   2.35410
