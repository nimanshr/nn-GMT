# Nsamples: 78  MAD:  0.668  mean:  0.024  std:  0.890
gfz2010mdti   0.30569
gfz2010rljv   0.31567
gfz2010tmba   0.12943
gfz2010uhjy   0.22873
gfz2010uidg  -0.28685
gfz2010xpzq   0.57272
gfz2010zjun   0.17583
gfz2011ahvh  -0.04446
gfz2011bpqm  -0.62942
gfz2011bygu  -0.13706
gfz2011dpyu  -1.12420
gfz2011fzcw   1.53000
gfz2011gnbk   0.15857
gfz2011kjzt  -0.70808
gfz2011kjzv  -0.58511
gfz2011ndhh  -0.52923
gfz2011nilw  -1.01490
gfz2011nlum  -0.42926
gfz2011nsiy  -0.16983
gfz2011ofpj  -0.21399
gfz2011oslk  -1.24510
gfz2011paqj  -1.34450
gfz2011pfwq   3.53920
gfz2011qukj   0.37408
gfz2011sdax   0.38376
gfz2011sxno  -0.05158
gfz2011tqgg   0.51222
gfz2012sqsc  -0.32212
gfz2012suqd   0.63309
gfz2012svwz   0.24390
gfz2012sxbo   1.51730
gfz2012tvgw   0.81048
gfz2012uegq   0.08409
gfz2012uhtc  -0.21922
gfz2012ulei   0.78297
gfz2012uovh  -0.52374
gfz2012uvoa  -0.73888
gfz2012uzww  -0.42200
gfz2012wowu   0.33424
gfz2013cyvx   0.37912
gfz2013fulo  -0.64452
gfz2013lbod  -1.14090
gfz2013lbvl  -1.19150
gfz2013ljdu   0.04274
gfz2013mdxj  -0.15025
gfz2013njwj  -0.72392
gfz2013oizq   0.41710
gfz2013quwz  -0.36439
gfz2013qxof  -0.40284
gfz2013uwjo   1.17160
gfz2013vmsg   1.99900
gfz2013yqtu   0.24667
gfz2013yxhf  -0.13974
gfz2013zfbz  -0.50716
gfz2013zlak  -0.26587
gfz2014ayxf   0.15152
gfz2014bkra   2.66320
gfz2014ctdl   2.11930
gfz2014djhi  -0.16049
gfz2014eoay  -0.48642
gfz2014epfm   0.98015
gfz2014epnq  -0.74686
gfz2014epue   1.61260
gfz2014esez  -1.07580
gfz2014faex  -0.44229
gfz2014fbli   0.94072
gfz2014fbnr   0.13538
gfz2014fdhk  -0.87053
gfz2014flxk   0.03437
gfz2014fsir  -0.52759
gfz2014gnbw  -0.24380
gfz2014igrp   0.12520
gfz2014iltl  -0.55234
gfz2014ined  -0.65102
gfz2014irlf  -0.82541
gfz2014irln  -1.11300
gfz2014iusw  -0.09867
gfz2014jarg   0.32360
