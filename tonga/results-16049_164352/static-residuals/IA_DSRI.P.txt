# Nsamples: 59  MAD:  0.617  mean:  0.065  std:  0.875
gfz2010drit  -1.53400
gfz2010drun  -1.53940
gfz2010dtgc   2.42220
gfz2010fqnn  -1.13160
gfz2010fxut   0.58786
gfz2010huzl   1.77620
gfz2010izag   0.52397
gfz2010mqaj   1.79210
gfz2010rljv   0.33695
gfz2010tzma   0.25843
gfz2010wakc  -0.81204
gfz2010xpzq   0.09049
gfz2011bygs   0.64564
gfz2011dpyu  -0.55394
gfz2011fszl   0.07019
gfz2011gnbk   0.15274
gfz2011qenl   0.75763
gfz2011scma   0.72596
gfz2011taqr   0.50390
gfz2011tsgl   0.00898
gfz2011vtzv  -0.13273
gfz2011zghc  -0.07209
gfz2012cvfr   0.39897
gfz2012ghiv   0.90926
gfz2012iijk   0.31807
gfz2012ldjj   0.06101
gfz2012nnqu  -0.86711
gfz2012ofnx  -0.04775
gfz2012phns  -0.16257
gfz2012sgfd   1.06880
gfz2012suqd   0.89310
gfz2012uhtc  -0.08691
gfz2012uvoa  -0.31080
gfz2012wpen  -0.03809
gfz2012xncm  -0.14197
gfz2012xxdj  -0.19225
gfz2013akkd   0.55512
gfz2013byyy  -3.41480
gfz2013dzvr   0.38045
gfz2013efct  -0.07964
gfz2013fulo   0.02694
gfz2013hbmi   0.52095
gfz2013iwzn   1.59140
gfz2013kbbg  -0.11464
gfz2013kbbn  -0.58221
gfz2013kcdb  -1.02320
gfz2013kpcb  -0.26450
gfz2013lbvl  -0.17077
gfz2013qigb  -0.32049
gfz2013qoak   0.24318
gfz2013quwz  -0.40834
gfz2013qxof   0.91926
gfz2013rwkj  -0.48083
gfz2013utqa  -0.56671
gfz2013wyfl   0.44318
gfz2013xahp  -0.17430
gfz2013xswc  -0.55401
gfz2013yqtu   0.27318
gfz2013zfbz   0.36523
