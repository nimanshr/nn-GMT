# Nsamples: 64  MAD:  0.794  mean: -0.061  std:  0.718
gfz2010abfc   0.57883
gfz2010aggj   0.02911
gfz2010cdnv  -1.00760
gfz2010clbw  -0.32677
gfz2010crgy  -0.86465
gfz2010dask   0.06674
gfz2010habf  -1.39900
gfz2010jlvp   0.59631
gfz2010mqzz   2.00410
gfz2010pzza  -0.31694
gfz2010qrix   0.55167
gfz2010rljv   0.66866
gfz2010uhjy   0.25143
gfz2010xpzq   1.19690
gfz2011dpyu  -0.32836
gfz2011gnbk  -0.15351
gfz2011lrpv   0.59294
gfz2011ndhh  -0.41624
gfz2011nfcz   0.58414
gfz2011niuw  -0.79734
gfz2011nuqo   0.95265
gfz2011ofpj   0.32246
gfz2011oslk  -1.40540
gfz2011owqa  -0.79739
gfz2011sdax  -0.83525
gfz2011sxno  -0.09546
gfz2011tsgl   0.56360
gfz2011wzss   0.34839
gfz2011zghc   1.08860
gfz2012bqcf  -0.88405
gfz2012cntu   0.49526
gfz2012cvfr  -0.57530
gfz2012ekfp  -0.36781
gfz2012gnow  -0.18020
gfz2012iijk  -1.37950
gfz2012kmbo  -0.59616
gfz2012mrqk   1.27600
gfz2012xxdj   0.22379
gfz2012zhyz   0.03505
gfz2013cyvx  -0.23056
gfz2013fulo  -0.82929
gfz2013fvyr   0.55105
gfz2013hbmi  -0.20856
gfz2013iuyv   0.22228
gfz2013jfcn   0.05590
gfz2013kats  -0.97837
gfz2013kbbg  -1.25910
gfz2013mdxj   0.00000
gfz2013phhi   0.84925
gfz2013qigb  -0.49379
gfz2013qoak   0.36216
gfz2013sdkg   0.10287
gfz2013utqa   1.15570
gfz2013wyfl   0.39671
gfz2013zlak  -0.70240
gfz2014bqpm   0.30942
gfz2014flxk  -0.86294
gfz2014fsir  -0.57167
gfz2014fsje   0.18548
gfz2014fxte   0.08044
gfz2014gnbw  -0.35105
gfz2014iltl  -0.64037
gfz2014irlf  -0.42040
gfz2014iusw  -0.30378
