# Nsamples: 47  MAD:  0.660  mean:  0.121  std:  0.674
gfz2010cllh  -0.60240
gfz2010crgy  -0.64766
gfz2010dask  -0.31840
gfz2010jlvp  -0.21118
gfz2010pvnn   0.26238
gfz2010pzza   0.24665
gfz2010uhjy  -0.56518
gfz2010xnbn   0.21937
gfz2011bpqm  -0.46298
gfz2011dabj  -0.54818
gfz2011dpyu  -0.10179
gfz2011gnbk   0.70226
gfz2011hjof  -0.21651
gfz2011hqjs  -0.38540
gfz2011ndhh  -0.62815
gfz2011sdax   0.36715
gfz2011uqry  -0.69823
gfz2012bqcf  -1.08560
gfz2012cvfr   1.09220
gfz2012ekfp  -0.31242
gfz2012kmbo  -0.48126
gfz2012oafg   0.29259
gfz2012uvoa  -0.50189
gfz2012uzww   0.48423
gfz2012wgmg   0.91253
gfz2013akkd   0.48422
gfz2013byyy   0.15376
gfz2013cyvx   1.26890
gfz2013dzvr   1.00670
gfz2013iuyv   1.40420
gfz2013jfcn  -0.02897
gfz2013jobl   0.06971
gfz2013kats  -0.40229
gfz2013kbbg  -0.05172
gfz2013kqab   1.23320
gfz2013oizq   1.71830
gfz2013phhi   0.52316
gfz2013utqa  -0.46778
gfz2013vmrv   0.95528
gfz2013wyfl   1.39920
gfz2013xahp   0.57780
gfz2014fsir   0.36983
gfz2014fxte  -0.17698
gfz2014icov  -0.27841
gfz2014iltl  -0.35406
gfz2014irlf  -0.50356
gfz2014irln  -0.03619
