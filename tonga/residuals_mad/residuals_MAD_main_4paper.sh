#!/bin/bash

gmtset PS_MEDIA 7.2cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/1.5p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

INFILE=../results-16049_164352/residuals-rms-mad.dat

ps=TONGA-RESIDUALS-MAD.ps


# #############################################################################
#          HISTOGRAM >>> CONFIG                                               #
# #############################################################################

R=0.5/7.5/0.32/0.45
J=X5c/5c

Bx=a1+l"Number of iteration"
By=a0.03+l"Travel time residual MAD [s]"
ANNOT=eWnS

S=c0.12
G=black
W=thick,black


# #############################################################################
#          HISTOGRAM >>> CONFIG                                               #
# #############################################################################
# --- Initialize ---
psxy -R$R -J$J -T -X0 -Y0 -K > $ps


awk '{print $1, $3}' $INFILE | psxy -R$R -J$J -W$W -Bx"$Bx" -By"$By" -B$ANNOT \
    -X1.6c -Y1.2c -O -K >> $ps

awk '{print $1, $3}' $INFILE | psxy -R -J -S$S -G$G -W$W -O -K >> $ps


# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert PS to PDF ---
psconvert $ps -Tf -P -A0.2


rm gmt.conf gmt.history

