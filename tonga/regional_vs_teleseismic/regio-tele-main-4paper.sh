#!/bin/bash

gmtset PS_MEDIA 15.6cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET 4p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

GMTFILE=./results_test/regio-vs-tele-locations.lonlat.gmt
HVFILE=./results_test/regio-vs-tele-locations.lonlat.hv

SLAB=./results_test/slab1.0_ker_top.in

TITLE0="Regional (with SSSTs)"
ABBREV0=regio

TITLE1="Teleseismic"
ABBREV1=tele


ps=TONGA-REGIO-VS-TELE.ps


# #############################################################################
#      MAP VIEW >>> CONFIG                                                    #
# #############################################################################
R1=176.5/-30.5/-168.25/-14r
J1=M4.5c
B1=a5f2.5

R2=-10/700/-30.1/-14
J2=X1.9c/5.25c
Bx2=a300f50+l"Depth [km]"
By2=a5f2.5+u"\\232"

R3=176.5/191.75/-10/700
J3=X4.5c/-1.9c
cat > Xannotes1.txt << EOF
175  a  175\\232
180  a  180\\232
185  a -175\\232
190  a -170\\232
195  a -165\\232
EOF
Bx3=Xannotes1.txt
By3=a300f50+l"Depth [km]"

SHIFT=0.25


# Hypocenters
Sr=c0.1
Wr=thinnest,blue2 #red2 #royalblue4
St=d0.1
Wt=thinnest,gray15

# Offset lines
Wl=thinnest,BLACK


DRY=150 #GRAY70
WET=220 #235 #LIGHTCYAN

F=+f2p,Helvetica-Bold,BLACK+jBL


# #############################################################################
# --- INITIALIZE ---
psxy -R$R1 -J$J1 -T -K -X0 -Y0> $ps


# #############################################################################
#      MAP VIEW >>> PLOT                                                      #
# #############################################################################

Xdummy=$(echo "4.5+$SHIFT" | bc -l)c
Ydummy=$(echo "1.9+$SHIFT" | bc -l)c
XOFFSETa=(1.4c  $Xdummy  -$Xdummy)
YOFFSETa=(3.1c  0  -$Ydummy)

# ########## Lat-Lon Map ##########
X=${XOFFSETa[0]}
Y=${YOFFSETa[0]}

grdimage -R$R1 -J$J1 ../topo/tonga_west.grd -I../topo/tonga_west.norm \
    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
pscoast -R$R1 -J$J1 -S$WET -B$B1 -BeWNs -N1 -Wthinnest -X0 -Y0 -O -K >> $ps
psbasemap -R -J -L-170.6/-28.2/-22/200 -O -K >> $ps

# Offset Lines
awk '{print $1, $2}' $GMTFILE | psxy -R -J -W$Wl -A -O -K >> $ps

# Hypocenters; Regional
awk '2 ==  NR % 3 {print $1, $2}' $GMTFILE | psxy -R -J -S$Sr -W$Wr -O -K >> $ps

# Hypocenters; Teleseismic
awk '2 ==  (NR+2) % 3 {print $1, $2}' $GMTFILE | psxy -R -J -S$St -W$Wt -O -K >> $ps

# Slab
awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -A -O -K >> $ps
pstext -R -J -F+f7p,Helvetica-Bold,BLACK+a73+jTL -O -K >> $ps << EOF
-173.8 -22.8 Tonga Trench
EOF
pstext -R -J -M -F+f7p,Helvetica-Bold,BLACK+a73+jTL -O -K >> $ps << EOF
> -177.0 -29.3 8p 1c c
Kermadec Trench
>
EOF


# ########## Depth-Lat Map ##########
X=${XOFFSETa[1]}
Y=${YOFFSETa[1]}

psbasemap -R$R2 -J$J2 -Bx"$Bx2" -By"$By2" -BENs -X$X -Y$Y -O -K >> $ps
psbasemap -R$R2 -J$J2 -By1000 -Bw -O -K >> $ps

# Offset Lines
awk '{print $3, $2, $1}' $GMTFILE | awk '{print $1, $2'} | psxy -R -J -W$Wl -A -O -K >> $ps

# Hypocenters; Regional
awk '2 ==  NR % 3 {print $3, $2}' $GMTFILE | psxy -R -J -S$Sr -W$Wr -O -K >> $ps

# Hypocenters; Teleseismic
awk '2 ==  (NR+2) % 3 {print $3, $2}' $GMTFILE | psxy -R -J -S$St -W$Wt -O -K >> $ps


# ########## Lon-Depth Map ##########
X=${XOFFSETa[2]}
Y=${YOFFSETa[2]}

psbasemap -R$R3 -J$J3 -Bxc$Bx3 -By"$By3" -BeWnS -X$X -Y$Y -O -K >> $ps

# Offset Lines
awk '{
if ($1 < 0)
	print $1+360,$3
else
	print $1,$3}' $GMTFILE | psxy -R -J -W$Wl -A -O -K >> $ps

# Hypocenters; Regional
awk '2 ==  NR % 3 {
if ($1 < 0)
    print $1+360, $3
else
    print $1, $3}' $GMTFILE | psxy -R -J -S$Sr -W$Wr -O -K >> $ps

# Hypocenters; Relocated
awk '2 ==  (NR+2) % 3 {
if ($1 < 0)
    print $1+360, $3
else
    print $1, $3}' $GMTFILE | psxy -R -J -S$St -W$Wt -O -K >> $ps


# #############################################################################
#          LEGEND                                                             #
# #############################################################################

# LEGEND
cat > legend.txt << EOF
N 1
S 0 c 0.15c WHITE 0.25p 0.15c $TITLE0
S 0 d 0.15c WHITE 0.45p 0.15c $TITLE1
EOF

pslegend -R -J -Dx4.75c/1.9c/1.9c/TL -C0.1c/0.1c -L1.2 -F+gwhite legend.txt -O -K \
	--FONT_ANNOT_PRIMARY=8p >> $ps



# #############################################################################
#      HISTOGRAMS >>> CONFIG                                                  #
# #############################################################################
RHor=0/160/0/80
RVer=-120/120/0/40
Jh=X4.3c/3c

Wh=5
Zh=0
Gh=150
Lh=thin,25

BxHor=a40f10+l"Horizontal Offset [km]"
BxVer=a40f10+l"Vertical Offset (Z@-tele@--Z@-regio@-) [km]"
By=a20f5+l"Count"
ANNOT=eWnS

X0T=-7.7
Y0T=11.3
DY=1.4
F=+f8p,Helvetica,BLACK+jTL


# #############################################################################
XOFFSETb=(9.5c  0)
YOFFSETb=(0  4.4c)

# ########## Vertical Offsets ##########
i=0
X=${XOFFSETb[$i]}
Y=${YOFFSETb[$i]}
awk 'NR>4 {print $2}' $HVFILE | pshistogram -R$RVer -J$Jh -W$Wh -Z$Zh -L$Lh -G$Gh \
    -By"$By" -Bx"$BxVer" -B$ANNOT -X$X -Y$Y -O -K >> $ps

X=115
Y=37.5
DY=4.5
F=+f8.5,Helvetica,BLACK+jTR
echo "$X $Y N=`awk 'NR==1 {print $3}' $HVFILE`" | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-1*$DY | bc` MAD=`awk 'NR==2 {print $3}' $HVFILE`"   | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-2*$DY | bc` @~m@~=`awk 'NR==3 {print $3}' $HVFILE`"  | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-3*$DY | bc` @~s@~=`awk 'NR==4 {print $3}' $HVFILE`" | pstext -R -J -F$F -O -K >> $ps


# ########## Horizontal Offsets ##########
i=1
X=${XOFFSETb[$i]}
Y=${YOFFSETb[$i]}
awk 'NR>4 {print $1}' $HVFILE | pshistogram -R$RHor -J$Jh -W$Wh -Z$Zh -L$Lh -G$Gh \
    -By"$By" -Bx"$BxHor" -B$ANNOT -X$X -Y$Y -O -K >> $ps

X=155
Y=75
DY=9
F=+f8.5,Helvetica,BLACK+jTR
echo "$X $Y N=`awk 'NR==1 {print $2}' $HVFILE`" | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-1*$DY | bc` MAD=`awk 'NR==2 {print $2}' $HVFILE`"   | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-2*$DY | bc` @~m@~=`awk 'NR==3 {print $2}' $HVFILE`"  | pstext -R -J -F$F -O -K >> $ps
echo "$X `echo $Y-3*$DY | bc` @~s@~=`awk 'NR==4 {print $2}' $HVFILE`" | pstext -R -J -F$F -O -K >> $ps



# --- Finalize ---
psxy -R -J -T -O >> $ps

# --- Convert ps to PDF ---
psconvert $ps -Tf -P -A0.2

rm Xannotes1.txt legend.txt gmt.conf gmt.history

