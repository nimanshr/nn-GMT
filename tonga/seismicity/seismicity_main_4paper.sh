#!/bin/bash

gmtset PS_MEDIA 15.6cx23.0c
gmtset PS_PAGE_ORIENTATION portrait

gmtset MAP_FRAME_TYPE plain
gmtset MAP_LABEL_OFFSET 4p
gmtset MAP_TITLE_OFFSET -8p
gmtset MAP_ANNOT_OFFSET_PRIMARY 3p
gmtset MAP_ANNOT_OFFSET_SECONDARY 3p
gmtset MAP_TICK_LENGTH_PRIMARY 3p/2p

gmtset FONT_TITLE 10p,Helvetica,BLACK
gmtset FONT_ANNOT_PRIMARY 10p,Helvetica
gmtset FONT_LABEL 10p,Helvetica-Bold

# #############################################################################

LOCFILE0=../results-16049_164352/single-locations.lonlat
TITLE0="Single-Event Locations"
LOCFILE1=../results-16049_164352/ssst-locations.lonlat
TITLE1="Global SSST Locations"

SLAB=../results-16049_164352/slab1.0_ker_top.in


ps=TONGA-SEISMICITY.ps


# #############################################################################
#          MAP VIEW >>> CONFIG                                                #
# #############################################################################

#RMAP=173/-32/-168/-12r
RMAP=173/-30.2/-170/-13.8r
JMAP=M6c

DRY=150 #gray70
WET=220 #225 #lightgray #lightcyan
B=a5f2.5

Se=c0.09
We=thinnest,GRAY15

F=+f9p,Helvetica-Bold,BLACK


C0=(+180  -16.5)
E0=(-172  -21.5)
LABEL0=A

C1=(+179  -19.5)
E1=(-173  -24.0)
LABEL1=B

C2=(+177  -23.5)
E2=(-174  -26.9)
LABEL2=C


# Cross Sections
cat > profile_lines.lonlat << EOF
>
+180  -16.5
-172  -21.5
>
+179  -19.5
-173  -24.0
>
+177  -23.5
-174  -26.9
EOF


cat > profile_labels.txt << EOF

+179.8  -17.0  MR  A
-171.8  -21.5  ML  A@+'@+

+178.8  -19.5  MR  B
-172.8  -24.0  ML  B@+'@+

+176.8  -23.5  MR  C
-173.8  -26.9  ML  C@+'@+
EOF


# Surface Rectangles
cat > rectangles.lonlat << EOF
>
-174.0  -16.2
-171.5  -16.2
-171.5  -14.0
-174.0  -14.0
-174.0  -16.2
>
-175.0  -21.5
-172.5  -21.5
-172.5  -19.4
-175.0  -19.4
-175.0  -21.5
>
+180.0  -22.5
-176.5  -22.5
-176.5  -19.4
+180.0  -19.4
+180.0  -22.5
>
+178.0  -26.0
-178.3  -26.0
-178.3  -22.5
+178.0  -22.5
+178.0  -26.0
EOF


cat > rectangle_labels.txt << EOF
-171.5  -14.2  TR  1
-172.5  -19.6  TR  2
+180.1  -19.5  TL  3
+178.1  -22.6  TL  4
EOF

# #############################################################################

# --- Initialize ---
psxy -R$RMAP -J$JMAP -T -X0 -Y0 -K > $ps


# #############################################################################
#           MAP VIEW >>> DEPTH COLOR PALLETE                                  #
# #############################################################################
makecpt -Cno_green -T0/600/30 -I > t.cpt


# #############################################################################
#           MAP VIEW >>> PLOT                                                 #
# #############################################################################
XOFFSET=(1.1c  7.4c)
YOFFSET=(15.2c  0)

for k in 0 1;do

	if [ $k = 0 ];then
    	ANNOT=eWnS+t$TITLE0
	else
    	ANNOT=EwnS+t$TITLE1
	fi

	X=${XOFFSET[$k]}
	Y=${YOFFSET[$k]}
	LOCFILE=`eval "echo $"LOCFILE$k""`

    grdimage -R$RMAP -J$JMAP ../topo/tonga_west.grd -I../topo/tonga_west.norm \
	    -C../topo/GMT_gray.cpt -X$X -Y$Y -O -K >> $ps
	pscoast -R$RMAP -J$JMAP -S$WET -Di -N1 -Wthinnest -X0 -Y0 -O -K >> $ps
	psbasemap -R -J -L175/-28.5/-22/200 -B"$B" -B"$ANNOT" -O -K --MAP_TICK_LENGTH_PRIMARY=5p/2.5p >> $ps

	# Epicenters
	awk '{print $4, $5, $6}' $LOCFILE | psxy -R -J -S$Se -Ct.cpt -W$We -O -K >> $ps

	# Slab
	awk '{print $1, $2}' $SLAB | psxy -R -J -Sf0.36/0.12+l+t -Gblack -O -K >> $ps

    # Cross Section Lines
    psxy -R -J -Wthin -A -O -K profile_lines.lonlat >> $ps

    # Cross Section Labels
    pstext -R -J -F+f9p,Helvetica-Bold,BLACK+j -O -K profile_labels.txt >> $ps

    # Surface Rectangles
    psxy -R -J -Wthin -A -O -K rectangles.lonlat >> $ps

    # Rectangle Labels
    pstext -R -J -F+f9p,Helvetica-Bold,BLACK+j -O -K rectangle_labels.txt >> $ps

done

# Colorbar
psscale -Dx-0.7c/1.5+w13.4c/0.25+jTC+h+ef -B:Depth\ \[km\]: -Ct.cpt -Y-2.3c -O -K >> $ps



# #############################################################################
#      CROSS SECTIONS >>> CONFIG                                              #
# #############################################################################
RCROSS=0/1000/-30/750
JCROSS=X5c/-2.5c

Bx=a250f50
By=a250f50

Se=c0.08
Ge=GRAY10
We=thinnest,BLACK

PROFWIDTH=90   # in kilometers


# #############################################################################
#      CROSS SECTIONS >>> PLOT                                                #
# #############################################################################

function plot_profiles {
	X0=$1        # X_origin of first subplot
	Y0=$2        # Y_origin of second subplot
	LOCFILE=$3   # hypocenter locations file
	TITLE=$4     # plot title

	DY=-3c

	N=0
	for i in 0 1 2;do
	    eval C=(\${"C"$i[@]})
	    cx=${C[0]}
	    cy=${C[1]}

	    eval E=(\${"E"$i[@]})
	    bx=${E[0]}
	    by=${E[1]}
		awk '{print $4, $5, $6}' $LOCFILE | project -C$cx/$cy -E$bx/$by \
			-W-$PROFWIDTH/$PROFWIDTH -Lw -Q -Fpz > proj$N.dat
		N=`expr $N + 1`
	done

	# Top subplot
	psbasemap -R$RCROSS -J$JCROSS -Bx$Bx -By$By -BeWns+t"$TITLE" -X$X0 -Y$Y0 -O -K \
	    --MAP_TITLE_OFFSET=4p >> $ps
	awk '{print}' proj0.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $ps

    # Labels
    LABEL=$LABEL0
	pstext -R -J -F$F+j -N -O -K >> $ps << EOF
	800  650  BL  ${LABEL}-${LABEL}@+'@+
EOF

	# Bottom subplots
	for i in 1 2;do
	    if [ $i = 2 ];then
            psbasemap -R$RCROSS -J$JCROSS -Bx$Bx+l"Distance [km]" -By$By -BeWnS -Y$DY -O -K >> $ps
	    else
            psbasemap -R$RCROSS -J$JCROSS -Bx$Bx -By$By+l"Depth [km]" -BeWns -Y$DY -O -K >> $ps
	    fi
	    awk '{print}' proj$i.dat | psxy -R -J -S$Se -G$Ge -W$We -O -K >> $ps

        # Labels
        LABEL=`eval "echo $"LABEL$i""`
	    pstext -R -J -F$F+j -N -O -K >> $ps << EOF
	    800  650  BL  ${LABEL}-${LABEL}@+'@+
EOF
	done
}

plot_profiles -6.9c -3.0c $LOCFILE0 "$TITLE0"
plot_profiles  7.4c  6.0c $LOCFILE1 "$TITLE1"



# #############################################################################
#          FIGURE LABELS                                                      #
# #############################################################################
gmt pstext -R0/10/0/11 -JX1i -F+f14p,Helvetica-Bold+jCB -N -X-7.4c -Y8.5c -O -K >> $ps << END
-4.5 40.2 (a)
-4.5  1.4 (b)
END



# #############################################################################


# --- Finalize ---
psxy -R -J -T -O >> $ps


# Convert ps file to PDF
psconvert $ps -Tf -P -A0.1

rm profile_lines.lonlat profile_labels.txt
rm rectangles.lonlat rectangle_labels.txt
rm t.cpt proj*.dat
rm gmt.conf gmt.history

